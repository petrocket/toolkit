local DeactivateAfterTimeout = 
{
	Properties = {
		Timeout = { default = 1, suffix = " sec"}
	},
	DeactivateTime = 0,
}

function DeactivateAfterTimeout:OnActivate()
	self.tickHandler = TickBus.Connect(self,0)
	local time = TickRequestBus.Broadcast.GetTimeAtCurrentTick()
	self.DeactivateTime = time:GetSeconds() + self.Properties.Timeout
end

function DeactivateAfterTimeout:OnTick(deltaTime, scriptTime)
	if scriptTime:GetSeconds() > self.DeactivateTime then
		GameEntityContextRequestBus.Broadcast.DeactivateGameEntity(self.entityId)
		self.tickHandler:Disconnect()
		self.tickHandler = nil
	end
end

function DeactivateAfterTimeout:OnDeactivate()
	if self.tickHandler ~= nil then
		self.tickHandler:Disconnect()
	end
end

return DeactivateAfterTimeout
