-- To use this system you need to create a lua file named EventNames.lua in your scripts folder
-- This file should return an object with all the event names like so
-- return {
--     EventName1 = "EventName1,
--     EventName2 = "EventName2"
--     ...
-- }
-- Usage:
-- at the top of your Lua file add
-- local Events = require "scripts/ToolKit/events"
-- To send an event (for example EventName1 with a value of 1) add
-- Events:Event(entityId, Events.EventName1, 1)

local Events = require "scripts/EventNames" 
if Events == nil then
    Events = {}
end

-- add some other events for Utilities
Events.OnRequestProperty = "OnRequestProperty"
Events.OnReceiveProperty = "OnReceiveProperty"
Events.OnGameDataUpdated = "OnGameDataUpdated"

-- used by StateMachine
Events.OnStateChange = "OnStateChange"

function Events:Event(entityId, event, value)
	local id = GameplayNotificationId(entityId, event, "float")
	GameplayNotificationBus.Event.OnEventBegin(id, value)
end

function Events:GlobalEvent(event, value)
	local id = GameplayNotificationId(EntityId(0), event, "float")
	GameplayNotificationBus.Event.OnEventBegin(id, value)
end

return Events
