local BasicCameraController = {
	Properties = {
		MoveSpeed = { default = 10.0, suffix = "m/s" },
		MoveSpeedModifier = { default = 10.0 },
		RotationSpeed = { default = 0.2, suffix = "m/s" },
		Epsilon = { default = 0.0001 },
		SmoothFactor = { default = 0.5 },
		Enabled = true,
		SetEnabledEvent = { default="OnSetFlyCameraEnabled" }
	},
	InputEvents = {
		OnMoveForwardBack = {},
		OnMoveLeftRight = {},
		OnLookUpDown = {},
		OnLookLeftRight = {},
		OnSpeedModifier = {},
	},
	GameplayEvents = {
		SetEnabled = {}
	}
}

function BasicCameraController:OnActivate()
	self.moveDirection = Vector2(0,0)
	self.lookDirection = Vector2(0,0)
	self.moveModifier = 1
	
	self:BindInputEvents(self.InputEvents)
	
	local gameplayId = GameplayNotificationId(EntityId(0), self.Properties.SetEnabledEvent, "float")
	self.GameplayEvents.SetEnabled.listener = GameplayNotificationBus.Connect(self.GameplayEvents.SetEnabled, gameplayId)
	self.GameplayEvents.SetEnabled.Component = self
	
	self:SetEnabled(self.Properties.Enabled)
end

function BasicCameraController:GetTickOrder()
	return TickOrder.Default - 1
end

function BasicCameraController:OnTick(deltaTime, scriptTime)
	-- rotate
	local lookSq = self.lookDirection:GetLengthSq()
	if lookSq > self.Properties.Epsilon then
		local tm = TransformBus.Event.GetWorldTM(self.entityId)
		local translation = tm:GetPosition()
		local m = Matrix3x3.CreateFromTransform(tm)
		local angles = self:CreateAnglesYawPitchRoll(m)
		angles.x = angles.x + (-self.lookDirection.x * deltaTime * self.Properties.RotationSpeed)
		angles.y = angles.y + (-self.lookDirection.y * deltaTime * self.Properties.RotationSpeed)
		
		-- prevent roll
		angles.z = 0
		
		-- clamp pitch
		angles.y = Math.Clamp(angles.y, -1.5, 1.5)
		
		-- convert back to an orientation
		m = self:CreateOrientationYawPitchRoll(angles)
		tm = Transform.CreateFromMatrix3x3AndTranslation(m, translation)
		TransformBus.Event.SetWorldTM(self.entityId, tm)
		
		-- framerate dependent smoothing (TODO make independent of fps)
		self.lookDirection.x = self.lookDirection.x * self.Properties.SmoothFactor
		self.lookDirection.y = self.lookDirection.y * self.Properties.SmoothFactor
	end
		
	-- translate
	local moveSq = self.moveDirection:GetLengthSq()
	if moveSq > self.Properties.Epsilon then
		local tm = TransformBus.Event.GetWorldTM(self.entityId)
		local move = Vector3(self.moveDirection.x ,self.moveDirection.y ,0)
		move = move:GetNormalized() * self.Properties.MoveSpeed * self.moveModifier * deltaTime
		TransformBus.Event.MoveEntity(self.entityId, (tm.basisX * move.x) + (tm.basisY * move.y))
		
		-- framerate dependent smoothing (TODO make independent of fps)
		self.moveDirection.x = self.moveDirection.x * self.Properties.SmoothFactor
		self.moveDirection.y = self.moveDirection.y * self.Properties.SmoothFactor
	end	
end

function BasicCameraController:CreateAnglesYawPitchRoll(m)
	local l = Vector3(m:GetElement(0,1), m:GetElement(1,1), 0.0):GetLength();
    if l > 0.0001 then
        return Vector3(
			Math.ArcTan2(-m:GetElement(0,1) / l, m:GetElement(1,1) / l), 
			Math.ArcTan2(m:GetElement(2,1), l), 
			Math.ArcTan2(-m:GetElement(2,0) / l, m:GetElement(2,2) / l))
    else
        return Vector3(0, Math.ArcTan2(m:GetElement(2,1), l), 0);
    end
end

function BasicCameraController:CreateOrientationYawPitchRoll(ypr)
	local sz = Math.Sin(ypr.x)
	local cz = Math.Cos(ypr.x)
	local sx = Math.Sin(ypr.y)
	local cx = Math.Cos(ypr.y)
	local sy = Math.Sin(ypr.z)
	local cy = Math.Cos(ypr.z)
	local c = Matrix3x3()
	c:SetElement(0,0, cy * cz - sy * sz * sx)
	c:SetElement(0,1, -sz * cx)
	c:SetElement(0,2, sy * cz + cy * sz * sx)
	c:SetElement(1,0, cy * sz + sy * sx * cz)
	c:SetElement(1,1, cz * cx)
	c:SetElement(1,2, sy * sz - cy * sx * cz)
	c:SetElement(2,0, -sy * cx)
	c:SetElement(2,1, sx)
	c:SetElement(2,2, cy * cx)
    return c
end

function BasicCameraController:OnDeactivate()
	self:UnBindInputEvents(self.InputEvents)

	if self.GameplayEvents.SetEnabled.listener ~= nil then
		self.GameplayEvents.SetEnabled.listener:Disconnect()
		self.GameplayEvents.SetEnabled.listener = nil
	end
	
	if self.tickHandler ~= nil then
		self.tickHandler:Disconnect()
		self.tickHandler = nil
	end
end

function BasicCameraController:SetEnabled(enabled)
	self.Properties.Enabled = enabled
	
	-- connecting/disconnecting from the tickbus effectively enables/disables the camera
	if enabled then
		if self.tickHandler == nil then
			self.tickHandler = TickBus.Connect(self,0)
		end 
	else
		if self.tickHandler ~= nil then
			self.tickHandler:Disconnect()
			self.tickHandler = nil			
		end
	end
end

function BasicCameraController.GameplayEvents.SetEnabled:OnEventBegin(enabled)
	self.Component:SetEnabled(enabled)
end

-- input events
function BasicCameraController:BindInputEvents(events)
	for event, handler in pairs(events) do
		handler.Component = self
		handler.Listener = InputEventNotificationBus.Connect(handler, InputEventNotificationId(event))
	end
end

function BasicCameraController:UnBindInputEvents(events)
	for event, handler in pairs(events) do
		handler.Listener:Disconnect()
		handler.Listener = nil
	end
end

function BasicCameraController.InputEvents.OnMoveForwardBack:OnHeld(value)
	self.Component.moveDirection.y = value
end

function BasicCameraController.InputEvents.OnMoveForwardBack:OnReleased(value)
	-- disabled this in favor of slowing down exponentially (slightly smoother)
	--self.Component.moveDirection.y = 0
end

function BasicCameraController.InputEvents.OnMoveLeftRight:OnHeld(value)
	self.Component.moveDirection.x = value
end

function BasicCameraController.InputEvents.OnMoveLeftRight:OnReleased(value)
	-- disabled this in favor of slowing down exponentially (slightly smoother)
	--self.Component.moveDirection.x = 0
end

function BasicCameraController.InputEvents.OnLookUpDown:OnHeld(value)
	self.Component.lookDirection.y = value
end

function BasicCameraController.InputEvents.OnLookUpDown:OnReleased(value)
	-- disabled this in favor of slowing down exponentially (slightly smoother)
	--self.Component.lookDirection.y = 0
end

function BasicCameraController.InputEvents.OnLookLeftRight:OnHeld(value)
	self.Component.lookDirection.x = value
end

function BasicCameraController.InputEvents.OnLookLeftRight:OnReleased(value)
	-- disabled this in favor of slowing down exponentially (slightly smoother)
	--self.Component.lookDirection.x = 0
end

function BasicCameraController.InputEvents.OnSpeedModifier:OnHeld(value)
	self.Component.moveModifier = self.Component.Properties.MoveSpeedModifier
end

function BasicCameraController.InputEvents.OnSpeedModifier:OnReleased(value)
	self.Component.moveModifier = 1.0
end

return BasicCameraController