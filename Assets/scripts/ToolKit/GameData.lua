local Events = require "scripts/ToolKit/Events"

-- You must create a Lua file named GameDataDefaults in your scripts folder that looks like this
-- return {
-- 	Names={
-- 		GameKey1="GameKey1"
-- 		GameKey2="GameKey2"
-- 		...
-- 	},
-- 	Values={
-- 		-- Default values for game keys (optional)
-- 		GameKey1=0,
-- 		GameKey2="Hello World,
-- 		...
-- 	}
-- }
--
local Defaults = require "scripts/GameDataDefaults"

if _G["GameData"] == nil then
	_G["GameData"] = Defaults.Values
	if _G["GameData"] == nil then
		_G["GameData"] = {}
	end
end

	 
local GameData = {}
if Defaults ~= nil then
	GameData = Defaults.Names
end

function GameData:Get(key)
	if _G["GameData"] ~= nil then
		return _G["GameData"][key]
	else
		return 0
	end
end

function GameData:Set(key,value)
	local oldValue = _G["GameData"][key]
	_G["GameData"][key] = value
	
	-- notify when data changes
	if type(value) == "table" or oldValue ~= value then
		--if self._DoNotLogField[key] == nil then
			--Debug.Log("Setting new value for " .. tostring(key) .. " DoNotLogField " .. tostring(self._DoNotLogField[key]))
		--end
		
		Events:GlobalEvent(Events.OnGameDataUpdated, key)

		-- notify what game data has changed
		--local id = GameplayNotificationId(EntityId(0), Events.OnGameDataUpdated, "float")
		--GameplayNotificationBus.Event.OnEventBegin(id, key)
		
		-- send a specific notification event
		--local id = GameplayNotificationId(EntityId(0), Events.OnGameDataUpdated .. tostring(key), "float")
		if type(value) == "table" then
			value = "table"
			--GameplayNotificationBus.Event.OnEventBegin(id, "table")
		end

		--if self._DoNotLogField[key] == nil then
		--	Debug.Log("Sending notification " .. tostring(Events.OnGameDataUpdated .. tostring(key)))
		--end
		--GameplayNotificationBus.Event.OnEventBegin(id, value)
		Events:GlobalEvent(Events.OnGameDataUpdated .. tostring(key), value)
	end
end

return GameData
