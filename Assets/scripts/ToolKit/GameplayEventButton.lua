local Utilities = require "scripts/ToolKit/Utilities"
local Events = require "scripts/ToolKit/Events"

local GameplayEventButton = {
	Properties = {
		Event = "",
		Value = "",
		Debug = false
	}
}

function GameplayEventButton:OnActivate()
	Utilities:InitLogging(self, "GameplayEventButton")
	self.buttonHandler = UiButtonNotificationBus.Connect(self, self.entityId)
	
	self.canvasId = UiElementBus.Event.GetCanvas(self.entityId)
	--self:Log("Entity ID " .. tostring(self.entityId))
	self:Log("Canvas ID " .. tostring(self.canvasId))	
end

function GameplayEventButton:OnDeactivate()
	self.buttonHandler:Disconnect()
end

function GameplayEventButton:OnButtonClick()
	self:Log("OnButtonClick " .. tostring(self.Properties.Event) .. " " .. tostring(self.Properties.Value))
	Events:GlobalEvent(self.Properties.Event, tostring(self.Properties.Value))
	--local id = GameplayNotificationId(EntityId(0), self.Properties.Event, "float")
	--GameplayNotificationBus.Event.OnEventBegin(id, tostring(self.Properties.Value))
end

return GameplayEventButton
