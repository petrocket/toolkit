local Utilities = require "scripts/ToolKit/utilities"
local GameData = require "scripts/ToolKit/gamedata"
local Events = require "scripts/ToolKit/events"

local GameDataField = {
	Properties = {
		TriggerEvent = "",
		GameDataField = "",
		GameDataSubField = "",
		Prefix = "",
		Suffix = "",
		Debug = false
	},
}

function GameDataField:OnActivate()
	Utilities:InitLogging(self, "GameDataField")
	if self.Properties.TriggerEvent ~= "" then
		self.Listener = GameplayNotificationBus.Connect(self, GameplayNotificationId(EntityId(0), self.Properties.TriggerEvent, "float"))
	else
		self.Listener = GameplayNotificationBus.Connect(self, GameplayNotificationId(EntityId(0), Events.OnGameDataUpdated .. tostring(self.Properties.GameDataField), "float"))
	end
	
	self:UpdateUI()
end

function GameDataField:OnDeactivate()
	if self.Listener ~= nil then
		self.Listener:Disconnect()
		self.Listener = nil
	end
end

function GameDataField:UpdateUI()
	local text = self.Properties.Prefix
	if self.Properties.GameDataField ~= "" then
		local data = GameData:Get(self.Properties.GameDataField)
		if data ~= nil then
			if self.Properties.GameDataSubField ~= "" then
				text = text .. tostring(data[self.Properties.GameDataSubField])
			else
				text = text .. tostring(data)
			end
			
			self:Log("Updating value for field " .. tostring(self.Properties.GameDataField) .. " " .. tostring(self.Properties.GameDataSubField) .. text)
			UiTextBus.Event.SetText(self.entityId, tostring(text) .. tostring(self.Properties.Suffix))
		end
	end
end

function GameDataField:OnEventBegin(value)
	self:UpdateUI()
end

return GameDataField
