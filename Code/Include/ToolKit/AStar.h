#pragma once
		
#include <AzCore/std/tuple.h>
#include <AzCore/Math/Vector3.h>
#include <AzCore/std/containers/vector.h>
#include <AzCore/RTTI/ReflectContext.h>

namespace ToolKit
{
	// x = AStarNodeId % gridWidth
	// y = (AStarNodeId - x) / gridHeight
	using AStarNodeId = AZ::u16; // this limits the max grid size such that width * height < max AZ::u16
	using AStarNodePoint = AZStd::tuple<AZ::u16, AZ::u16>;

	struct AStarNode
	{
		//AZ_TYPE_INFO(AStarNode, "{3A8888ED-2570-46F7-933A-B6C095CFF675}");
		AZ::u32 m_inOpenList           : 1;
		AZ::u32 m_isObstacle           : 1;
		AZ::u32 m_visited              : 1;
		AZ::u32 padding                : 13;
		AZ::u32 m_costFromStart        : 16;

		AZ::u32 m_totalEstimatedCost   : 16;
		AStarNodeId m_prevPathNode;

		AStarNodeId m_prevOpenListNode;
		AStarNodeId m_nextOpenListNode;
	};

	struct AStarGrid
	{
		AZ::u16 m_width;
		AZ::u16 m_height;
		AZStd::vector<AStarNode> m_nodes;
	};

	class AStarSolver
	{
	public:
		//AZ_RTTI(AStarSolver, "{D44D28A4-31E0-4DDF-B894-BEF8E9F56FA3}")
		//AZ_CLASS_ALLOCATOR_DECL

		//AStarSolver() = default;
		AStarSolver(AZ::u16 gridWidth, AZ::u16 gridHeight);

		static void Reflect(AZ::ReflectContext* context);

		void SetObstacle(AStarNodeId nodeId, bool isObstacle = true);

		// might be simpler to just re-create the whole AStarSolver?  this may just memset(0...) the grid nodes
		void ClearObstacles();

		AZStd::vector<AStarNodeId> FindPath(AStarNodeId startNodeId, AStarNodeId endNodeId);

		AZ::u16 GetGridWidth() { return m_grid.m_width;  }
		AZ::u16 GetGridHeight() { return m_grid.m_height;  }
		AStarNodeId GetNodeId(AZ::u16 x, AZ::u16 y) { return m_grid.m_width * y + x; }
		AStarNodePoint GetNodePosition(AStarNodeId nodeId)
		{ 
			const AZ::u16 x = nodeId % m_grid.m_width;
			const AZ::u16 y = (nodeId - x) / m_grid.m_width;
			return AStarNodePoint(x, y);
		}

	private:
		AZ::u16 GetCostBasedOnDistance(AStarNodeId startNodeId, AStarNodeId endNodeId);
		bool NeighborIsOffGrid(AStarNodeId nodeId, int direction);
		AStarNodeId GetNodeId(AStarNode* node);

		AStarGrid m_grid;
	};
}
