
#pragma once

#include <AzCore/EBus/EBus.h>
#include <LyShine/UiBase.h>
#include <LmbrCentral/Scripting/TagComponentBus.h>
#include <AzCore/Asset/AssetCommon.h>
#include <ToolKit/AStar.h>

// forward declarations
namespace AZ
{
    class EntityId;
    class Vector2;
    class Vector3;
    class Vector4;
}

namespace ToolKit
{
	// AI
	using NavigationRequestId = AZ::u32;

    class ToolKitRequests
        : public AZ::EBusTraits
    {
    public:
        static const AZ::EBusHandlerPolicy HandlerPolicy = AZ::EBusHandlerPolicy::Single;
        static const AZ::EBusAddressPolicy AddressPolicy = AZ::EBusAddressPolicy::Single;


		// ai
		virtual NavigationRequestId FindPathToEntity(const AZ::EntityId& fromEntityId, const AZ::EntityId& toEntityId) = 0;
		virtual NavigationRequestId FindPathToLocation(const AZ::Vector3& fromLocation, const AZ::Vector3& destination) = 0;

		virtual AZStd::vector<AZ::Vector2> FindPath(AZ::u16 startX, AZ::u16 startY, AZ::u16 endX, AZ::u16 endY) = 0;
		virtual void CreateNavigationGrid(AZ::u16 gridWidth,  AZ::u16 gridHeight) = 0;
		virtual void SetNavigationObstacle(AZ::u16 x, AZ::u16 y, bool isObstacle) = 0;
		virtual void ClearNavigationObstacles() = 0;

        // entities 
        virtual AZStd::vector<AZ::EntityId> GetEntitiesWithTag(const LmbrCentral::Tag& tag) = 0;
        virtual void DeactivateEntityRecursive(const AZ::EntityId& entityId) = 0;
        virtual void ActivateEntityRecursive(const AZ::EntityId& entityId) = 0;

        // graphics
        virtual AZ::Vector2 ProjectToScreen(const AZ::Vector3& position) = 0;
        virtual AZ::Vector3 ProjectEntityRadiusToScreen(const AZ::EntityId& entityId) = 0;
        virtual AZ::Vector4 ProjectEntityBoundsToScreen(const AZ::EntityId& entityId) = 0;
        virtual AZ::Vector3 UnProjectFromScreen(const AZ::Vector2& position, float screenDepth) = 0;
        virtual void CloneExistingMaterial(const AZ::EntityId& entityId) = 0;
        virtual void RestoreMaterial(const AZ::EntityId& entityId) = 0;
        virtual void SetMaterialDiffuseTexture(const AZ::EntityId& entityId, const AZStd::string& texture) = 0;
		virtual bool AABBIsVisible(const AZ::EntityId& entityId) = 0;
		virtual void SetViewShake(const AZ::Vector3& angle, const AZ::Vector3& shakeShift, float duration, float frequency, float randomness, int shakeID, bool flipVec, bool updateOnly, bool groundOnly) = 0;

        // networking
        virtual bool IsDedicatedServer() = 0;
        virtual bool IsHost() = 0;

        // editor
        virtual bool IsEditor() = 0;

        // physics
        virtual bool IsOnGround(const AZ::EntityId& entityId) = 0;
        virtual float WaterLevel(const AZ::EntityId& entityId) = 0;
        virtual void SetVelocityAndAngularVelocity(const AZ::EntityId& entityId, const AZ::Vector3& velocity, const AZ::Vector3& angularVelocity) = 0;
        virtual void SetFreeFallDamping(const AZ::EntityId& entityId, const float damping) = 0;
		virtual AZ::Vector3 GetAimDirectionForTarget(const AZ::Vector3& projectileOrigin, float projectileSpeed, const AZ::Vector3& targetPosition, const AZ::Vector3& targetVelocity) = 0;
        virtual AZStd::vector<AZ::EntityId> OverlapBox(const AZ::Transform& transform, const AZ::Vector3& dimensions, AZ::EntityId entityToIgnore) = 0;

        // console
        virtual void ExecuteCommand(const  AZStd::string& command) = 0;
        virtual AZStd::string GetCVar(const  AZStd::string& cvarName) = 0;
        
        // environment
        virtual void SetOceanVisible(const bool& visible) = 0;
        virtual void SetTerrainVisible(const bool& visible) = 0;
        virtual void SetTimeOfDay(const float& hour) = 0;
        virtual void LoadTimeOfDay(const AZStd::string& path) = 0;
        virtual void SetSkyMaterial(const AZStd::string& path) = 0;
        virtual void SetSunPosition(const float& longitude, const float& latitude) = 0;
        virtual void SetDawnDusk(const float& dawnStart, const float& dawnEnd, const float& duskStart, const float& duskEnd) = 0;

        // UI
        virtual AZ::Vector2 GetMousePosition(bool normalized = true) = 0;
        virtual AZ::Vector2 GetWindowResolution() = 0;
        virtual AZ::EntityId GetEntityUnderCursor() = 0;
        virtual AZ::Vector3 GetPointUnderCursor() = 0;
        virtual void DrawDebugText2D(const AZStd::string& id, const AZStd::string& text, float x, float y, float fontSize, const AZ::Vector3& color, float duration) = 0;

        // IO
        virtual AZStd::string GetPathToExe() = 0;

        // resolve a path (can resolve aliases too like @root@, @assets@, @user@ etc - see ISystem.h for full list of aliases)
        virtual AZStd::string GetFullPath(const AZStd::string& path) = 0;
        // shortcut for @user@
        virtual AZStd::string GetUserPath() = 0;
        // shortcut for @root@
        virtual AZStd::string GetRootPath() = 0;
        // shortcut for @log@
        virtual AZStd::string GetLogPath() = 0;
        // shortcut for @assets@
        virtual AZStd::string GetAssetsPath() = 0;

        virtual AZStd::string GetPathToAsset(const AZ::Data::AssetId& asselId) = 0;

        // get the assetId from a path
        virtual AZ::Data::AssetId GetAssetId(const AZStd::string& path) = 0;

        // get file modification time
        virtual AZ::u64 GetModificationTime(const AZStd::string& path) = 0;
    };

    using ToolKitRequestBus = AZ::EBus<ToolKitRequests>;

    class ToolKitNotifications
        : public AZ::EBusTraits
    {
    public:
        // AI 
		virtual void OnNavPathFound(NavigationRequestId id, const AZStd::vector<AZ::Vector3>& pathPoints) = 0;
    };
    using ToolKitNotificationBus = AZ::EBus<ToolKitNotifications>;


    // We use a specialized bus address (bus ID) that is a combination of entity Id and resource Crc because
    // a common use case for this bus would be something like a health resource or ammo resource.
    // When say a weapon is asked if it has any ammo, typically you are asking a specific entity and 
    // asking about a type of resource (ammo), so it makes sense to combine these into a single bus address.
    // If we were to use no address or only the entity id as an address then you would have to check the 
    // resource type every time you got a notification to see if it was the resource you were interested in 
    // and this could negatively effect performance  especially for resources that change rapidly like ammo, 
    // health, shields, oxygen etc.
    // 
    // Optionally, you can designate EntityId(0) as the global entity address and resourceCrc(0) as the global
    // resource CRC and use these to listen for events for all resource notifications for an entity, or for
    // a resource.  Downside being that you would may not know which entity or resource was changed.
    class ResourceBusId
    {
    public:
        AZ_TYPE_INFO(ResourceBusId, "{F6E7736D-C849-4CFB-9A93-FCF8B9E1AB7A}");
        ResourceBusId() = default;
        ResourceBusId(const AZ::EntityId& entityChannel, AZ::Crc32 resourceCrc)
            : m_channel(entityChannel)
            , m_resourceCrc(resourceCrc)
        { }

        ResourceBusId Clone() const { return *this; }

        bool operator==(const ResourceBusId& rhs) const 
        { 
            return m_channel == rhs.m_channel && m_resourceCrc == rhs.m_resourceCrc; 
        }

        AZStd::string ToString() const
        {
            return AZStd::string::format("(channel=%llu, resourceCrc=%lu)", static_cast<AZ::u64>(m_channel), static_cast<AZ::u32>(m_resourceCrc));
        }

        AZ::EntityId m_channel;
        AZ::Crc32 m_resourceCrc;
    };


    class ResourceRequests
        : public AZ::EBusTraits
    {
    public:
        static const AZ::EBusAddressPolicy AddressPolicy = AZ::EBusAddressPolicy::ById;
        typedef ResourceBusId BusIdType;

        virtual ~ResourceRequests() = default;

        virtual void Add(float amount) = 0;
        virtual float Get() = 0;
        virtual void Set(float amount) = 0;
        virtual float GetMin() = 0;
        virtual void SetMin(float minAmount) = 0;
        virtual float GetMax() = 0;
        virtual void SetMax(float maxAmount) = 0;
    };
    using ResourceRequestBus = AZ::EBus<ResourceRequests>;


    class ResourceNotifications
        : public AZ::EBusTraits
    {
    public:
        static const AZ::EBusAddressPolicy AddressPolicy = AZ::EBusAddressPolicy::ById;
        typedef ResourceBusId BusIdType;

        virtual ~ResourceNotifications() = default;

        virtual void OnAmountChanged(float oldAmount, float newAmount) = 0;
    };
    using ResourceNotificationBus = AZ::EBus<ResourceNotifications>;

} // namespace ToolKit

namespace AZStd
{
	template <>
	struct hash < ToolKit::ResourceBusId >
	{
		inline size_t operator()(const ToolKit::ResourceBusId& resourceBusId) const
		{
			AZStd::hash<AZ::EntityId> entityIdHasher;
			size_t retVal = entityIdHasher(resourceBusId.m_channel);
			AZStd::hash_combine(retVal, resourceBusId.m_resourceCrc);
			return retVal;
		}
	};
} // namespace AZStd
