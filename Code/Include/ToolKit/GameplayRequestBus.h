#pragma once

#include <AzCore/EBus/EBus.h>
#include <AzCore/std/any.h>

namespace AZ
{
    class GameplayNotificationId;
}

namespace ToolKit
{
    class GameplayRequests
        : public AZ::EBusTraits
    {
    public:
        static const AZ::EBusAddressPolicy AddressPolicy = AZ::EBusAddressPolicy::ById;
        typedef AZ::GameplayNotificationId BusIdType;
        virtual ~GameplayRequests() = default;
        virtual float OnEventRequestFloat(const AZStd::any& param) = 0;
    };
    using GameplayRequestBus = AZ::EBus<GameplayRequests>;
} // namespace ToolKit
