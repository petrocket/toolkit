
#pragma once

#include <functor.h> // needed in <INavigationSystem.h>
#include <ISerialize.h>
#include <IPathfinder.h>

// for INavigationSystem access
#include <ISystem.h>
#include <IAISystem.h>

#include <AzCore/Component/Component.h>
#include <AzFramework/Input/Devices/Touch/InputDeviceTouch.h>
#include <AzFramework/Input/Buses/Notifications/InputChannelNotificationBus.h>
#include <ToolKit/ToolKitBus.h>

struct ray_hit;
// Forward-declare legacy AI interfaces to avoid an include dependency on IPathfinder.h.
using IPathFollowerPtr = AZStd::shared_ptr<IPathFollower>;
using INavPathPtr = AZStd::shared_ptr<INavPath>;

namespace ToolKit
{
    class ToolKitSystemComponent
        : public AZ::Component
        , protected ToolKitRequestBus::Handler
        , protected AzFramework::InputChannelNotificationBus::Handler
        , public IAIPathAgent
    {
    public:
        AZ_COMPONENT(ToolKitSystemComponent, "{EC1BC068-2251-42B8-9BDF-0FEC15D32142}");

        static void Reflect(AZ::ReflectContext* context);

        static void GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided);
        static void GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible);
        static void GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required);
        static void GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent);

    protected:
        void OnInputChannelEvent(const AzFramework::InputChannel& inputChannel, bool& o_hasBeenConsumed) override;

        // Callback for AI System Nav path finding result
        void OnPathResult(const MNM::QueuedPathID& pathfinderRequestId, MNMPathRequestResult& result);

        ////////////////////////////////////////////////////////////////////////
        // ToolKitRequestBus interface implementation

        // AI
        ToolKit::NavigationRequestId FindPathToEntity(const AZ::EntityId& fromEntityId, const AZ::EntityId& toEntityId) override;
        ToolKit::NavigationRequestId FindPathToLocation(const AZ::Vector3& fromLocation, const AZ::Vector3& destination) override;
		AZStd::vector<AZ::Vector2> FindPath(AZ::u16 startX, AZ::u16 startY, AZ::u16 endX, AZ::u16 endY) override;
		void CreateNavigationGrid(AZ::u16 gridWidth, AZ::u16 gridHeight) override;
		void SetNavigationObstacle(AZ::u16 x, AZ::u16 y, bool isObstacle) override;
		void ClearNavigationObstacles() override;

        // entities 
        AZStd::vector<AZ::EntityId> GetEntitiesWithTag(const LmbrCentral::Tag& tag) override;
        void DeactivateEntityRecursive(const AZ::EntityId& entityId) override;
        void ActivateEntityRecursive(const AZ::EntityId& entityId) override;

        // graphics
        AZ::Vector2 ProjectToScreen(const AZ::Vector3& position) override;
        AZ::Vector3 ProjectEntityRadiusToScreen(const AZ::EntityId& entityId) override;
        AZ::Vector4 ProjectEntityBoundsToScreen(const AZ::EntityId& entityId) override;
        AZ::Vector3 UnProjectFromScreen(const AZ::Vector2& position, float screenDepth) override;

        // clone the existing material used by an entity so that the parameters can be changed without
        // changing every other mesh that uses that same material
        void CloneExistingMaterial(const AZ::EntityId& entityId) override;
        void RestoreMaterial(const AZ::EntityId& entityId) override;
        bool AABBIsVisible(const AZ::EntityId& entityId) override;

        void SetViewShake(const AZ::Vector3& angle, const AZ::Vector3& shakeShift, float duration, float frequency, float randomness, int shakeID, bool flipVec, bool updateOnly, bool groundOnly) override;

        // change the diffuse texture for the exiting material 
        void SetMaterialDiffuseTexture(const AZ::EntityId& entityId, const AZStd::string& texture) override;

        // networking
        bool IsDedicatedServer() override;
        bool IsHost() override;

        // editor
        bool IsEditor() override;

        // physics
        bool IsOnGround(const AZ::EntityId& entityId) override;
        float WaterLevel(const AZ::EntityId& entityId) override;
        void SetVelocityAndAngularVelocity(const AZ::EntityId& entityId, const AZ::Vector3& velocity, const AZ::Vector3& angularVelocity) override;
        void SetFreeFallDamping(const AZ::EntityId& entityId, const float damping) override;
		AZ::Vector3 GetAimDirectionForTarget(const AZ::Vector3& projectileOrigin, float projectileSpeed, const AZ::Vector3& targetPosition, const AZ::Vector3& targetVelocity) override;
        AZStd::vector<AZ::EntityId> OverlapBox(const AZ::Transform& transform, const AZ::Vector3& dimensions, AZ::EntityId entityToIgnore) override;

        // console
        void ExecuteCommand(const  AZStd::string& command) override;
        AZStd::string GetCVar(const  AZStd::string& cvarName) override;

        // environment
        void SetOceanVisible(const bool& visible) override;
        void SetTerrainVisible(const bool& visible) override;
        void SetTimeOfDay(const float& hour) override;
        void LoadTimeOfDay(const AZStd::string& path) override;
        void SetSkyMaterial(const AZStd::string& path) override;
        void SetSunPosition(const float& longitude, const float& latitude) override;
        void SetDawnDusk(const float& dawnStart, const float& dawnEnd, const float& duskStart, const float& duskEnd) override;

        // UI
        AZ::Vector2 GetMousePosition(bool normalized = true) override;
        AZ::Vector2 GetWindowResolution() override;
        AZ::EntityId GetEntityUnderCursor() override;
        AZ::Vector3 GetPointUnderCursor() override;
        void DrawDebugText2D(const AZStd::string& id, const AZStd::string& text, float x, float y, float fontSize, const AZ::Vector3& color, float duration) override;

        // IO
        AZStd::string GetPathToExe() override;

        // @root@ To get to the folder where system.cfg lives
        // @assets@ to get to the folder containing game assets (textures and such) - by default, this is @root@/Gamename/
        // @devroot@ to get to source files that are checked into source control (PC EDITOR ONLY!)
        // @engroot@ to get to path to the engine root folder
        // @user@ to access user store
        // @cache@ to access temporary cache
        // @log@ to access log file and other forensic storage
        AZStd::string GetFullPath(const AZStd::string& path) override;
        AZStd::string GetUserPath() override;
        AZStd::string GetRootPath() override;
        AZStd::string GetLogPath() override;
        AZStd::string GetAssetsPath() override;
        AZStd::string GetPathToAsset(const AZ::Data::AssetId& asselId) override;

        AZ::Data::AssetId GetAssetId(const AZStd::string& path) override;
        AZ::u64 GetModificationTime(const AZStd::string& path) override;

        ////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////
        // AZ::Component interface implementation
        void Init() override;
        void Activate() override;
        void Deactivate() override;
        ////////////////////////////////////////////////////////////////////////

    protected:
        //// IAIPathAgent
        IEntity * GetPathAgentEntity() const override { return nullptr; }
        const char* GetPathAgentName() const override { return "ToolKitAgent"; }
        void GetPathAgentNavigationBlockers(NavigationBlockers&, const ::PathfindRequest*) override {}
        AZ::u16 GetPathAgentType() const override { return AIOBJECT_ACTOR; }
        Vec3 GetPathAgentPos() const override { return Vec3(); }
        float GetPathAgentPassRadius() const override { return 10.f; }
        Vec3 GetPathAgentVelocity() const override { return ZERO; }
        AZ::u32 GetPathAgentLastNavNode() const override { return 0; }
        void SetPathAgentLastNavNode(unsigned int) override {}
        void SetPathToFollow(const char*) override {}
        void SetPathAttributeToFollow(bool) override {}
        void SetPFBlockerRadius(int, float) override {}
        ETriState CanTargetPointBeReached(CTargetPointRequest&) override { return eTS_maybe; }
        bool UseTargetPointRequest(const CTargetPointRequest&) override { return false; }
        bool GetValidPositionNearby(const Vec3&, Vec3&) const override { return false; }
        bool GetTeleportPosition(Vec3&) const override { return false; }
        class IPathFollower* GetPathFollower() const override { return nullptr; }
        bool IsPointValidForAgent(const Vec3&, AZ::u32) const { return true; };
        const AgentMovementAbility& GetPathAgentMovementAbility() const
        {
            static AgentMovementAbility sTemp;
            return sTemp;
        }
        //// ~IAIPathAgent

    private:
        AZ::Vector2 m_lastTouchPosition;
        int RayCastFromCursor(ray_hit &hit);
        IPathFollowerPtr m_pathFollower;

		AZStd::unique_ptr<AStarSolver> m_aStarSolver;
        AZStd::unordered_map<AZ::EntityId, AZStd::vector<AZ::EntityId>> m_deactivatedEntities;
    };
}
