#pragma once



#include <AzFramework/Physics/PhysicsComponentBus.h>

using AzFrameworkCollision = AzFramework::PhysicsComponentNotifications::Collision;

#include <Source/ToolKitScriptCanvas.generated.h>

#include <ScriptCanvas/Core/Node.h>
#include <ScriptCanvas/CodeGen/CodeGen.h>
#include <ScriptCanvas/Libraries/Libraries.h>

namespace ToolKit
{
	// create a node library so the nodes show up in the ScriptCanvas node palette window
	struct ToolKitLibrary
		: public ScriptCanvas::Library::LibraryDefinition
	{
		AZ_RTTI(Debug, "{DFC87D3E-75A3-448A-9200-2AB33F32F14B}", ScriptCanvas::Library::LibraryDefinition);
		static void Reflect(AZ::ReflectContext*);
		static void InitNodeRegistry(ScriptCanvas::NodeRegistry& nodeRegistry);
		static AZStd::vector<AZ::ComponentDescriptor*> GetComponentDescriptors();
	};

	class Loop 
		: public ScriptCanvas::Node
	{
		ScriptCanvas_Node(Loop,
			ScriptCanvas_Node::Name("Loop")
			ScriptCanvas_Node::Uuid("{8E3DF7D7-599C-4F05-A0B6-63797470C98A}")
			ScriptCanvas_Node::Description("Triggers the output the specified number of times.")
		);

	public:

		Loop();

	protected:

		// Inputs
		ScriptCanvas_In(ScriptCanvas_In::Name("In", "Input signal"));

		// Outputs
		ScriptCanvas_Out(ScriptCanvas_Out::Name("Out", "Output"));
		ScriptCanvas_PropertyWithDefaults(int, 
			0,
			ScriptCanvas_Property::Name("Value", "The current numeric value")
			ScriptCanvas_Property::Output);
		ScriptCanvas_Out(ScriptCanvas_Out::Name("Complete", "Complete"));


		// Data
		ScriptCanvas_PropertyWithDefaults(int, 
			1,
			ScriptCanvas_Property::Name("Start", "The number to start at")
			ScriptCanvas_Property::Input);

		ScriptCanvas_PropertyWithDefaults(int, 
			1,
			ScriptCanvas_Property::Name("End", "The number to end at")
			ScriptCanvas_Property::Input);

		ScriptCanvas_PropertyWithDefaults(int,
			1,
			ScriptCanvas_Property::Name("Increment", "Increment by this amount each time, negative number means count down, default is 1")
			ScriptCanvas_Property::Input);

		void OnInputSignal(const ScriptCanvas::SlotId& slot) override;
	};

	class Loop2D 
		: public ScriptCanvas::Node
	{
		ScriptCanvas_Node(Loop2D,
			ScriptCanvas_Node::Name("Loop2D")
			ScriptCanvas_Node::Uuid("{6D519030-0C66-4D0F-8409-87C5E119B495}")
			ScriptCanvas_Node::Description("Iterates through a 2 dimensional loop and triggers the output the specified number of times.")
		);

	public:

		Loop2D();

	protected:

		// Inputs
		ScriptCanvas_In(ScriptCanvas_In::Name("In", "Input signal"));

		// Outputs
		ScriptCanvas_Out(ScriptCanvas_Out::Name("Out", "Output"));
		ScriptCanvas_PropertyWithDefaults(int, 
			0,
			ScriptCanvas_Property::Name("Value1", "Current numeric value for dimension 1")
			ScriptCanvas_Property::Output);
		ScriptCanvas_PropertyWithDefaults(int, 
			0,
			ScriptCanvas_Property::Name("Value2", "Current numeric value for dimension 2")
			ScriptCanvas_Property::Output);
		ScriptCanvas_Out(ScriptCanvas_Out::Name("Complete", "Complete"));

		// Data
		ScriptCanvas_PropertyWithDefaults(int, 
			1,
			ScriptCanvas_Property::Name("Start1", "The number to start at for dimension 1")
			ScriptCanvas_Property::Input);

		ScriptCanvas_PropertyWithDefaults(int, 
			1,
			ScriptCanvas_Property::Name("End1", "The number to end at for dimesnion 1")
			ScriptCanvas_Property::Input);

		ScriptCanvas_PropertyWithDefaults(int,
			1,
			ScriptCanvas_Property::Name("Increment1", "Increment dimension 1 by this amount each time, negative number means count down, default is 1")
			ScriptCanvas_Property::Input);

		ScriptCanvas_PropertyWithDefaults(int, 
			1,
			ScriptCanvas_Property::Name("Start2", "The number to start at for dimension 2")
			ScriptCanvas_Property::Input);

		ScriptCanvas_PropertyWithDefaults(int, 
			1,
			ScriptCanvas_Property::Name("End2", "The number to end at for dimesnion 2")
			ScriptCanvas_Property::Input);

		ScriptCanvas_PropertyWithDefaults(int,
			1,
			ScriptCanvas_Property::Name("Increment2", "Increment dimension 2 by this amount each time, negative number means count down, default is 1")
			ScriptCanvas_Property::Input);

		void OnInputSignal(const ScriptCanvas::SlotId& slot) override;
	};

	/*
	class All 
		: public ScriptCanvas::Node
	{
		ScriptCanvas_Node(All,
			ScriptCanvas_Node::Name("All")
			ScriptCanvas_Node::Uuid("{C83652DA-4859-4685-9C2C-E22BBBCA9D46}")
			ScriptCanvas_Node::Description("Triggers the output once all the inputs have been triggered once.")
		);

	public:

		All();

	protected:

		// Inputs
		ScriptCanvas_In(ScriptCanvas_In::Name("Reset", "Reset"));


		// Outputs
		ScriptCanvas_Out(ScriptCanvas_Out::Name("Out", "Signaled when the node has received a signal from every input"));

		void OnInit() override;
		void OnActivate() override;
		void OnConfigured() override;
		AZ::Crc32 GetInputExtensionId() const { return AZ_CRC("Output"); }

		bool CanDeleteSlot(const ScriptCanvas::SlotId& slotId) const override;
		void OnSlotRemoved(const ScriptCanvas::SlotId& slotId) override;
		bool IsNodeExtendable() const override { return true; }
		int GetNumberOfExtensions() const override;
		ScriptCanvas::SlotId HandleExtension(AZ::Crc32 extensionId) override;
		ScriptCanvas::ExtendableSlotConfiguration GetExtensionConfiguration(int extensionCount) const override;

		void OnInputSignal(const ScriptCanvas::SlotId& slotId) override;
	private:
		AZStd::string GenerateInputName(int counter);
		ScriptCanvas::SlotId AddInputSlot();
		void FixupStateNames();

		AZStd::unordered_set<ScriptCanvas::SlotId> m_triggeredInputs;
		bool m_triggeredOutput;
		ScriptCanvas::SlotId m_resetSlotId;
	};
	*/

	class GetCollisionInfo 
		: public ScriptCanvas::Node
	{
		ScriptCanvas_Node(GetCollisionInfo,
			ScriptCanvas_Node::Name("GetCollisionInfo")
			ScriptCanvas_Node::Uuid("{E342B10F-ABE2-468C-B126-274412F24581}")
			ScriptCanvas_Node::Description("Gets information about a recent collision.")
		);

	public:
		GetCollisionInfo() = default;

	protected:
		// Inputs
		ScriptCanvas_In(ScriptCanvas_In::Name("In", "Input signal"));

		// Data
		ScriptCanvas_Property(AzFrameworkCollision,
			ScriptCanvas_Property::Name("Collision", "The collision")
			ScriptCanvas_Property::Input);

		// Outputs
		ScriptCanvas_Out(ScriptCanvas_Out::Name("Out", "Signaled when the node has received a signal from every input"));

		ScriptCanvas_PropertyWithDefaults(AZ::EntityId,
			AZ::EntityId(),
			ScriptCanvas_Property::Name("EntityId", "The EntityId of the entity that was collided with if any.")
			ScriptCanvas_Property::Output);

		void OnInputSignal(const ScriptCanvas::SlotId& slotId) override;
	};
}
