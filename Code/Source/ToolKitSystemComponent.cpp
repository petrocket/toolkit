
#include "StdAfx.h"

#include <AzCore/Serialization/SerializeContext.h>
#include <AzCore/Serialization/EditContext.h>

#include "ToolKitSystemComponent.h"
#include "ToolKitScriptCanvas.h"
#include "ToolKit/GameplayRequestBus.h"

#include <AzCore/RTTI/BehaviorContext.h>
#include <AzCore/Asset/AssetManagerBus.h>
#include <AzCore/Component/EntityId.h>
#include <AzCore/Component/ComponentApplicationBus.h>
#include <AzCore/Component/TransformBus.h>
#include <AzCore/Math/Transform.h>
#include <AzCore/std/string/string.h>
#include <AzCore/Math/Vector2.h>
#include <AzCore/Math/Vector4.h>
#include <AzCore/IO/FileIO.h>

#include <AzFramework/Components/CameraBus.h>
#include <AzFramework/Input/Buses/Requests/InputSystemCursorRequestBus.h>
#include <AzFramework/Input/Devices/Mouse/InputDeviceMouse.h>
#include <AzFramework/Asset/AssetCatalogBus.h>
#include <AzFramework/Physics/World.h>
#include <AzFramework/Physics/ShapeConfiguration.h>
#include <AzFramework/Entity/GameEntityContextBus.h>

#include <LyShine/UIBase.h>
#include <LyShine/Bus/UiCanvasBus.h>
#include <LyShine/Bus/UiTextBus.h>
#include <LyShine/Bus/UiImageBus.h>
#include <LyShine/Bus/UiTransformBus.h>
#include <LyShine/Animation/IUiAnimation.h>

#include <LmbrCentral/Rendering/MaterialOwnerBus.h>
#include <LmbrCentral/Rendering/MeshComponentBus.h>
#include <LmbrCentral/Physics/PhysicsComponentBus.h>
#include <LmbrCentral/Physics/CryPhysicsComponentRequestBus.h>
#include <LmbrCentral/Rendering/RenderBoundsBus.h>


// copied from stdafx.h of Cry3DEngine system
#define MAX_PATH_LENGTH 512

#include "I3DEngine.h"
#include "Cry3DEngineBase.h"
#include "cvars.h"
#include "3dEngine.h"
#include "IConsole.h"
#include "IMovieSystem.h"
#include "IGameFramework.h"
#include "ITimeOfDay.h"
#include <MathConversion.h>
#include "IGame.h"
#include "IViewSystem.h"
//#include "IHardwareMouse.h"
#include "GameplayEventBus.h"

#pragma optimize("", off)

namespace ToolKit
{
    // Behavior Context forwarder for AssetBus 
    /*
    class BehaviorToolKitAssetBusHandler : public AZ::Data::AssetBus::MultiHandler, public AZ::BehaviorEBusHandler
    {
    public:
        AZ_EBUS_BEHAVIOR_BINDER(BehaviorToolKitAssetBusHandler, "{7437A60F-BDB8-4EE0-B9CF-7E5893C34E90}", AZ::SystemAllocator,
            OnAssetReady,
            OnAssetReloaded,
            OnAssetReloadError,
            OnAssetError);


        void OnAssetReady(AZ::Data::Asset<AZ::Data::AssetData> asset) override
        {
            Call(FN_OnAssetReady, asset);
        }

        void OnAssetReloaded(AZ::Data::Asset<AZ::Data::AssetData> asset) override
        {
            Call(FN_OnAssetReloaded, asset);
        }

        void OnAssetReloadError(AZ::Data::Asset<AZ::Data::AssetData> asset) override
        {
            Call(FN_OnAssetReloadError, asset);
        }

        /// Signal that an asset error has occurred
        void OnAssetError(AZ::Data::Asset<AZ::Data::AssetData> asset) override
        {
            Call(FN_OnAssetError, asset);
        }
    };
    */

    /// BahaviorContext forwarder for BehaviorGameplayRequestBusHandler
    class BehaviorGameplayRequestBusHandler : public ToolKit::GameplayRequestBus::Handler, public AZ::BehaviorEBusHandler
    {
    public:
        AZ_EBUS_BEHAVIOR_BINDER(BehaviorGameplayRequestBusHandler, "{674756F3-B519-4891-859B-A4098BC07BCD}", AZ::SystemAllocator,
            OnEventRequestFloat);

        float OnEventRequestFloat(const AZStd::any& value) override
        {
            float result = -1.f;
            CallResult(result, FN_OnEventRequestFloat, value);
            return result;
        }
    };


    // Behavior Context forwarder for ToolKitNotificationBus
    class BehaviorToolKitNotificationBusHandler : public ToolKitNotificationBus::Handler, public AZ::BehaviorEBusHandler
    {
    public:
        AZ_EBUS_BEHAVIOR_BINDER(BehaviorToolKitNotificationBusHandler, "{B9E45B5E-17E9-4563-85EE-687D737BC850}", AZ::SystemAllocator,
            OnNavPathFound);

        void OnNavPathFound(ToolKit::NavigationRequestId id, const AZStd::vector<AZ::Vector3>& pathPoints) override
        {
            Call(FN_OnNavPathFound, id, pathPoints);
        }
    };

    // Behavior Context forwarder for ResourceNotificationBus 
    class BehaviorResourceNotificationBusHandler : public ResourceNotificationBus::Handler, public AZ::BehaviorEBusHandler
    {
    public:
        AZ_EBUS_BEHAVIOR_BINDER(BehaviorResourceNotificationBusHandler, "{A02F727B-46DA-47B8-B251-89CFF460ED43}", AZ::SystemAllocator,
            OnAmountChanged);

        void OnAmountChanged(float oldAmount, float newAmount) override
        {
            Call(FN_OnAmountChanged, oldAmount, newAmount);
        }

    };

    // Behavior Context forwarder for BehaviorResourceRequestBusHandler 
    class BehaviorResourceRequestBusHandler : public ResourceRequestBus::Handler, public AZ::BehaviorEBusHandler
    {
    public:
        AZ_EBUS_BEHAVIOR_BINDER(BehaviorResourceRequestBusHandler, "{ABD525A6-81A0-40B5-9F7B-F67BB435FB4E}", AZ::SystemAllocator,
            Get,
            Set,
            Add,
            GetMin,
            SetMin,
            GetMax,
            SetMax);

        float Get() override
        {
            float result = 0.f;
            CallResult(result, FN_Get);
            return result;
        }

        void Set(float amount) override
        {
            Call(FN_Set, amount);
        }

        void Add(float amount) override
        {
            Call(FN_Add, amount);
        }

        void SetMin(float amount) override
        {
            Call(FN_SetMin, amount);
        }
        float GetMin() override
        {
            float result = 0.f;
            CallResult(result, FN_GetMin);
            return result;
        }

        void SetMax(float amount) override
        {
            Call(FN_SetMax, amount);
        }
        float GetMax() override
        {
            float result = 0.f;
            CallResult(result, FN_GetMax);
            return result;
        }
    };

    namespace Internal
    {
        void ResourceBusIdDefaultConstructor(ResourceBusId* thisPtr)
		{
            new (thisPtr) ResourceBusId();
		}

        // special constructor for script that allows 0 or 2 parameters.   
        // (EntityId, Crc32) or (EntityId, String)
        void ResourceBusIdScriptConstructor(ResourceBusId* thisPtr, AZ::ScriptDataContext& dc)
        {
            int numArgs = dc.GetNumArguments();
            if (numArgs == 2)
            {
                if (dc.IsClass<AZ::EntityId>(0))
                {
                    AZ::EntityId entityId;
                    dc.ReadArg(0, entityId);

                    AZ::Crc32 crc;
					if (dc.IsClass<AZ::Crc32>(1))
					{
                        dc.ReadArg(0, crc);
                        *thisPtr = ResourceBusId(entityId, crc);
					}
					else if (dc.IsString(1))
					{
                        const char* str = nullptr;
                        if (dc.ReadArg(1, str))
                        {
                            *thisPtr = ResourceBusId(entityId, AZ::Crc32(str));
                        }
					}
                }
            }
            else if(numArgs == 0)
            {
                *thisPtr = ResourceBusId(AZ::EntityId(0), AZ::Crc32());
            }
            else
            {
                dc.GetScriptContext()->Error(AZ::ScriptContext::ErrorType::Error, true, "ResourceBusId() accepts only 0 or 2 arguments, not %d!", numArgs);
            }
        }
    }

    void ToolKitSystemComponent::Reflect(AZ::ReflectContext* context)
    {
        // reflect script canvas nodes
        ToolKitLibrary::Reflect(context);

        if (AZ::SerializeContext * serialize = azrtti_cast<AZ::SerializeContext*>(context))
        {
            serialize->Class<ToolKitSystemComponent, AZ::Component>()
                ->Version(0);

            serialize->Class<ResourceBusId>()
                ->Version(1)
                ->Field("EntityId", &ResourceBusId::m_channel)
                ->Field("ResourceType", &ResourceBusId::m_resourceCrc)
                ;

            if (AZ::EditContext * ec = serialize->GetEditContext())
            {
                ec->Class<ToolKitSystemComponent>("ToolKit", "[Description of functionality provided by this System Component]")
                    ->ClassElement(AZ::Edit::ClassElements::EditorData, "")
                    // ->Attribute(AZ::Edit::Attributes::Category, "") Set a category
                    ->Attribute(AZ::Edit::Attributes::AppearsInAddComponentMenu, AZ_CRC("System"))
                    ->Attribute(AZ::Edit::Attributes::AutoExpand, true)
                    ;
            }
        }

        if (AZ::BehaviorContext * behaviorContext = azrtti_cast<AZ::BehaviorContext*>(context))
        {
            /*
            behaviorContext->Class<AZ::Data::Asset<AZ::Data::AssetData>>()
                ->Method("GetId", &AZ::Data::Asset<AZ::Data::AssetData>::GetId)
                ->Method("GetHint", &AZ::Data::Asset<AZ::Data::AssetData>::GetHint)
                ;
                */

            behaviorContext->EBus<ToolKit::GameplayRequestBus>("GameplayRequestBus")
                ->Attribute(AZ::Script::Attributes::ExcludeFrom, AZ::Script::Attributes::ExcludeFlags::List)
                ->Handler<BehaviorGameplayRequestBusHandler>()
                ->Event("OnEventRequestFloat", &ToolKit::GameplayRequestBus::Events::OnEventRequestFloat);

            /*
            behaviorContext->EBus<AZ::Data::AssetBus>("AssetBus")
                ->Handler<BehaviorToolKitAssetBusHandler>()
                ->Event("OnAssetReady", &AZ::Data::AssetBus::Events::OnAssetReady)
                ->Event("OnAssetReloaded", &AZ::Data::AssetBus::Events::OnAssetReloaded)
                ->Event("OnAssetReloadError", &AZ::Data::AssetBus::Events::OnAssetReloadError)
                ->Event("OnAssetError", &AZ::Data::AssetBus::Events::OnAssetError);
                */

            behaviorContext->EBus<ToolKitRequestBus>("ToolKitRequestBus")
                ->Event("AABBIsVisible", &ToolKitRequestBus::Events::AABBIsVisible)
                ->Event("FindPathToEntity", &ToolKitRequestBus::Events::FindPathToEntity)
                ->Event("FindPathToLocation", &ToolKitRequestBus::Events::FindPathToLocation)
                ->Event("FindPath", &ToolKitRequestBus::Events::FindPath, {{
						{ "Start X", "The start x position in the grid" },
						{ "Start Y", "The start y position in the grid" },
						{ "End X", "The end x position in the grid" },
						{ "End Y", "The end y position in the grid" }
					}})
                ->Event("CreateNavigationGrid", &ToolKitRequestBus::Events::CreateNavigationGrid, {{
						{ "Width", "Grid width" },
						{ "Height", "Grid height" }
					}})
                ->Event("SetNavigationObstacle", &ToolKitRequestBus::Events::SetNavigationObstacle, {{
						{ "X", "Grid x position" },
						{ "Y", "Grid y position" },
						{ "IsObstacle", "True if this position is an obstacle" }
					}})
                ->Event("ClearNavigationObstacles", &ToolKitRequestBus::Events::ClearNavigationObstacles)
                ->Event("GetEntitiesWithTag", &ToolKitRequestBus::Events::GetEntitiesWithTag)
                ->Event("ActivateEntityRecursive", &ToolKitRequestBus::Events::ActivateEntityRecursive, {{
                        { "EntityId", "The entity to activate (and all it's decendants)" }
                        }})
                ->Event("DeactivateEntityRecursive", &ToolKitRequestBus::Events::DeactivateEntityRecursive, { {
                        { "EntityId", "The entity to deactivate (and all it's decendants)" }
                        }})
                ->Event("ProjectToScreen", &ToolKitRequestBus::Events::ProjectToScreen)
                ->Event("ProjectEntityBoundsToScreen", &ToolKitRequestBus::Events::ProjectEntityBoundsToScreen)
                ->Event("ProjectEntityRadiusToScreen", &ToolKitRequestBus::Events::ProjectEntityRadiusToScreen)
                ->Event("UnProjectFromScreen", &ToolKitRequestBus::Events::UnProjectFromScreen)
                ->Event("CloneExistingMaterial", &ToolKitRequestBus::Events::CloneExistingMaterial)
                ->Event("RestoreMaterial", &ToolKitRequestBus::Events::RestoreMaterial)
                ->Event("SetMaterialDiffuseTexture", &ToolKitRequestBus::Events::SetMaterialDiffuseTexture)
                ->Event("SetViewShake", &ToolKitRequestBus::Events::SetViewShake)
                ->Event("IsEditor", &ToolKitRequestBus::Events::IsEditor)
                ->Event("IsDedicatedServer", &ToolKitRequestBus::Events::IsDedicatedServer)
                ->Event("IsHost", &ToolKitRequestBus::Events::IsHost)
                ->Event("IsOnGround", &ToolKitRequestBus::Events::IsOnGround)
                ->Event("WaterLevel", &ToolKitRequestBus::Events::WaterLevel)
                ->Event("SetVelocityAndAngularVelocity", &ToolKitRequestBus::Events::SetVelocityAndAngularVelocity)
                ->Event("SetFreeFallDamping", &ToolKitRequestBus::Events::SetFreeFallDamping)
                ->Event("ExecuteCommand", &ToolKitRequestBus::Events::ExecuteCommand)
                ->Event("SetOceanVisible", &ToolKitRequestBus::Events::SetOceanVisible)
                ->Event("SetTerrainVisible", &ToolKitRequestBus::Events::SetTerrainVisible)
                ->Event("LoadTimeOfDay", &ToolKitRequestBus::Events::LoadTimeOfDay)
                ->Event("SetTimeOfDay", &ToolKitRequestBus::Events::SetTimeOfDay)
                ->Event("SetSkyMaterial", &ToolKitRequestBus::Events::SetSkyMaterial)
                ->Event("SetSunPosition", &ToolKitRequestBus::Events::SetSunPosition)
                ->Event("SetDawnDusk", &ToolKitRequestBus::Events::SetDawnDusk)
                ->Event("GetMousePosition", &ToolKitRequestBus::Events::GetMousePosition)
                ->Event("GetWindowResolution", &ToolKitRequestBus::Events::GetWindowResolution)
                ->Event("GetEntityUnderCursor", &ToolKitRequestBus::Events::GetEntityUnderCursor)
                ->Event("GetPointUnderCursor", &ToolKitRequestBus::Events::GetPointUnderCursor)
                ->Event("DrawDebugText2D", &ToolKitRequestBus::Events::DrawDebugText2D)
                ->Event("GetPathToExe", &ToolKitRequestBus::Events::GetPathToExe)
                ->Event("GetFullPath", &ToolKitRequestBus::Events::GetFullPath)
                ->Event("GetUserPath", &ToolKitRequestBus::Events::GetUserPath)
                ->Event("GetRootPath", &ToolKitRequestBus::Events::GetRootPath)
                ->Event("GetLogPath", &ToolKitRequestBus::Events::GetLogPath)
                ->Event("GetAssetsPath", &ToolKitRequestBus::Events::GetAssetsPath)
                ->Event("GetPathToAsset", &ToolKitRequestBus::Events::GetPathToAsset)
                ->Event("GetAssetId", &ToolKitRequestBus::Events::GetAssetId)
                ->Event("GetModificationTime", &ToolKitRequestBus::Events::GetModificationTime)
				->Event("GetAimDirectionForTarget", &ToolKitRequestBus::Events::GetAimDirectionForTarget, {{
						{ "Projectile Origin", "The projectile origin" },
						{ "Projectile Speed", "Speed of the projectile" },
						{"Target Position","The position of the target"},
						{"Target Velocity", "The velocity of the target"}
					}})
                ->Event("OverlapBox", &ToolKitRequestBus::Events::OverlapBox, {{
                        { "Transform", "The transform of the box" },
                        { "Dimensions", "The box dimensions" },
                        {"EntityId","Entity to ignore (optional)"}
					}})
                ;

            behaviorContext->EBus<ToolKitNotificationBus>("ToolKitNotificationBus")
                ->Handler<BehaviorToolKitNotificationBusHandler>()
                ->Event("OnNavPathFound", &ToolKitNotificationBus::Events::OnNavPathFound)
                ;

            behaviorContext->Class<ResourceBusId>("ResourceBusId")
                ->Constructor<AZ::EntityId, AZ::Crc32>()
                    ->Attribute(AZ::Script::Attributes::Storage, AZ::Script::Attributes::StorageType::Value)
                    ->Attribute(AZ::Script::Attributes::ConstructorOverride, &Internal::ResourceBusIdScriptConstructor)
                    ->Attribute(AZ::Script::Attributes::GenericConstructorOverride, &Internal::ResourceBusIdDefaultConstructor)
                ->Property("EntityId", BehaviorValueProperty(&ResourceBusId::m_channel))
                ->Property("ResourceType", BehaviorValueProperty(&ResourceBusId::m_resourceCrc))
                ->Method("Clone", &ResourceBusId::Clone)
                ->Method("Create", 
                    [](AZ::EntityId entityId, AZStd::string_view value) -> ResourceBusId { return ResourceBusId(entityId, AZ::Crc32(value)); }, 
                    { { 
                        { "Entity Id", "Entity Id" }, 
                        {"Resource Type", "The resource type"} 
                    } })
                ->Method("ToString", &ResourceBusId::ToString)
                    ->Attribute(AZ::Script::Attributes::Operator, AZ::Script::Attributes::OperatorType::ToString)
                ->Method("Equal", &ResourceBusId::operator==)
                    ->Attribute(AZ::Script::Attributes::Operator, AZ::Script::Attributes::OperatorType::Equal)
                ;

            behaviorContext->EBus<ResourceRequestBus>("ResourceRequestBus")
                ->Handler<BehaviorResourceRequestBusHandler>()
                ->Event("Set", &ResourceRequestBus::Events::Set, { {
                        { "Amount", "The new resource amount" }
                    } })
                ->Event("Get", &ResourceRequestBus::Events::Get)
                ->Event("Add", &ResourceRequestBus::Events::Add, { {
                        { "Amount", "The amount to add to this resource (negative to remove)" }
                    } })
                ->Event("GetMax", &ResourceRequestBus::Events::GetMax)
                ->Event("SetMax", &ResourceRequestBus::Events::SetMax, { {
                        { "Amount", "The new max for this resource" }
                    } })
                ->Event("GetMin", &ResourceRequestBus::Events::GetMin)
                ->Event("SetMin", &ResourceRequestBus::Events::SetMin, { {
                        { "Amount", "The new minimum for this resource" }
                    } })
                ;
            behaviorContext->EBus<ResourceNotificationBus>("ResourceNotificationBus")
                ->Handler<BehaviorResourceNotificationBusHandler>()
                ->Event("OnAmountChanged", &ResourceNotificationBus::Events::OnAmountChanged)
                ;
        }
    }

    void ToolKitSystemComponent::GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided)
    {
        provided.push_back(AZ_CRC("ToolKitService"));
    }

    void ToolKitSystemComponent::GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible)
    {
        incompatible.push_back(AZ_CRC("ToolKitService"));
    }

    void ToolKitSystemComponent::GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required)
    {
        (void)required;
    }

    void ToolKitSystemComponent::GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent)
    {
        (void)dependent;
    }

    void ToolKitSystemComponent::Init()
    {
        AZ::EnvironmentVariable<ScriptCanvas::NodeRegistry> nodeRegistryVariable = AZ::Environment::FindVariable<ScriptCanvas::NodeRegistry>(ScriptCanvas::s_nodeRegistryName);
        if (nodeRegistryVariable)
        {
            ScriptCanvas::NodeRegistry& nodeRegistry = nodeRegistryVariable.Get();
            ToolKitLibrary::InitNodeRegistry(nodeRegistry);
        }
    }

    ToolKit::NavigationRequestId ToolKitSystemComponent::FindPathToEntity(const AZ::EntityId& fromEntityId, const AZ::EntityId& toEntityId)
    {
        AZ::Vector3 startLocation;
        AZ::TransformBus::EventResult(startLocation, fromEntityId, &AZ::TransformBus::Events::GetWorldTranslation);

        AZ::Vector3 destination;
        AZ::TransformBus::EventResult(destination, toEntityId, &AZ::TransformBus::Events::GetWorldTranslation);

        return FindPathToLocation(startLocation, destination);
    }

    ToolKit::NavigationRequestId ToolKitSystemComponent::FindPathToLocation(const AZ::Vector3& fromLocation, const AZ::Vector3& destination)
    {
        // Create a new pathfind request
        MNMPathRequest pathfinderRequest;
        pathfinderRequest.startLocation = AZVec3ToLYVec3(fromLocation);
        pathfinderRequest.endLocation = AZVec3ToLYVec3(destination);
        // 3. Set the type of the Navigation agent (hardcoded for now!)
        const NavigationAgentTypeID agentTypeID = gEnv->pAISystem->GetNavigationSystem()->GetAgentTypeID("MediumSizedCharacters");
        pathfinderRequest.agentTypeID = agentTypeID;

        // 4. Set the callback
        pathfinderRequest.resultCallback = functor(*this, &ToolKitSystemComponent::OnPathResult);

        // 5. Request the path.
        IMNMPathfinder* pathFinder = gEnv->pAISystem ? gEnv->pAISystem->GetMNMPathfinder() : nullptr;
        return pathFinder ? pathFinder->RequestPathTo(this, pathfinderRequest) : 0;
    }

    void ToolKitSystemComponent::OnPathResult(const MNM::QueuedPathID& pathfinderRequestId, MNMPathRequestResult& result)
    {
        AZStd::vector<AZ::Vector3> pathPoints;
        if (result.HasPathBeenFound())
        {
            TPathPoints points = result.pPath->GetPath();
            for (PathPointDescriptor point : points)
            {
                pathPoints.push_back(LYVec3ToAZVec3(point.vPos));
            }
        }
        ToolKitNotificationBus::Broadcast(&ToolKitNotificationBus::Events::OnNavPathFound, result.id, pathPoints);
    }

	AZStd::vector<AZ::Vector2> ToolKitSystemComponent::FindPath(AZ::u16 startX, AZ::u16 startY, AZ::u16 endX, AZ::u16 endY)
	{
		AZStd::vector<AZ::Vector2> result;
		if (m_aStarSolver)
		{
			const AStarNodeId startNodeId = m_aStarSolver->GetNodeId(startX, startY);
			const AStarNodeId endNodeId = m_aStarSolver->GetNodeId(endX, endY);
			AZStd::vector<AStarNodeId> path = m_aStarSolver->FindPath(startNodeId, endNodeId);
			if (!path.empty())
			{
				result.reserve(path.size());
				for (auto nodeId : path)
				{
					AStarNodePoint point = m_aStarSolver->GetNodePosition(nodeId);
					result.emplace_back(AZ::Vector2(AZStd::get<0>(point), AZStd::get<1>(point)));
				}
			}
		}
		return result;
	}

	void ToolKitSystemComponent::CreateNavigationGrid(AZ::u16 gridWidth, AZ::u16 gridHeight)
	{
		m_aStarSolver = AZStd::make_unique<AStarSolver>(gridWidth, gridHeight);
	}

	void ToolKitSystemComponent::SetNavigationObstacle(AZ::u16 x, AZ::u16 y, bool isObstacle)
	{
		if (m_aStarSolver)
		{
			const AStarNodeId nodeId = m_aStarSolver->GetNodeId(x, y);
			m_aStarSolver->SetObstacle(nodeId, isObstacle);
		}
	}

	void ToolKitSystemComponent::ClearNavigationObstacles()
	{
		if (m_aStarSolver)
		{
			m_aStarSolver->ClearObstacles();
		}
	}

    void ToolKitSystemComponent::Activate()
    {
        ToolKitRequestBus::Handler::BusConnect();
        AzFramework::InputChannelNotificationBus::Handler::BusConnect();
    }

    void ToolKitSystemComponent::Deactivate()
    {
        ToolKitRequestBus::Handler::BusDisconnect();
        AzFramework::InputChannelNotificationBus::Handler::BusDisconnect();
    }

    AZStd::vector<AZ::EntityId> ToolKitSystemComponent::GetEntitiesWithTag(const LmbrCentral::Tag& tag)
    {
        AZ::EBusAggregateResults<AZ::EntityId> results;
        LmbrCentral::TagGlobalRequestBus::EventResult(results, tag, &LmbrCentral::TagGlobalRequestBus::Events::RequestTaggedEntities);
        return results.values;
    }

    void ToolKitSystemComponent::DeactivateEntityRecursive(const AZ::EntityId& entityId)
    {
        auto itr = m_deactivatedEntities.find(entityId);
        if (itr == m_deactivatedEntities.end())
        {
            AZStd::vector<AZ::EntityId> descendants;
            AZ::TransformBus::EventResult(descendants, entityId, &AZ::TransformBus::Events::GetAllDescendants);
            m_deactivatedEntities.insert({ entityId, descendants });

            // deactivate children first to preserve transforms
            for (AZStd::vector<AZ::EntityId>::reverse_iterator entityIdIter = descendants.rbegin();
                 entityIdIter != descendants.rend(); ++entityIdIter)
            {
                AzFramework::GameEntityContextRequestBus::Broadcast(&AzFramework::GameEntityContextRequestBus::Events::DeactivateGameEntity, *entityIdIter );
            }
            AzFramework::GameEntityContextRequestBus::Broadcast(&AzFramework::GameEntityContextRequestBus::Events::DeactivateGameEntity, entityId);
        }
    }

    void ToolKitSystemComponent::ActivateEntityRecursive(const AZ::EntityId& entityId)
    {
        auto itr = m_deactivatedEntities.find(entityId);
        if (itr != m_deactivatedEntities.end())
        {
            // activate parents first to preserve transforms
            AzFramework::GameEntityContextRequestBus::Broadcast(&AzFramework::GameEntityContextRequestBus::Events::ActivateGameEntity, entityId);
            for (AZ::EntityId descendantId : itr->second)
            {
                AzFramework::GameEntityContextRequestBus::Broadcast(&AzFramework::GameEntityContextRequestBus::Events::ActivateGameEntity, descendantId);
            }

            m_deactivatedEntities.erase(itr);
        }
    }

    namespace Internal
    {
        const CCamera& GetCamera()
        {
            // we don't want to use the rendering camera because it isn't updated until right before rendering 
            // the order is
            // 1. PreUpdate
            // 2. Tick
            // 3. PostUpdate <-- View System updates the CrySystem render camera which is used for RenderWorld
            // so we must get the active view ourselves and use that camera
            AZ_Assert(gEnv->pGame, "Global environment game not set");
            IGameFramework* gameFramework = gEnv->pGame->GetIGameFramework();
            AZ_Assert(gameFramework, "Game framework is null");
            IView* view = gameFramework->GetIViewSystem()->GetActiveView();
            if (!view)
            {
                // it is possible that this level doesn't have an active camera component in which case
                // we will just use the rendering camera and deal with it.
                return gEnv->p3DEngine->GetRenderingCamera();
            }
            return view->GetCamera();
        }

        AZ::Vector2 ProjectVectorToScreen(const AZ::Vector3& position)
        {
            AZ::Vector2 offScreen(-1024.f - 1024.f);

            if (gEnv && gEnv->pRenderer)
            {
                const CCamera& camera = Internal::GetCamera();
                float fov = camera.GetFov();
                float nearPlane = camera.GetNearPlane();
                float farPlane = camera.GetFarPlane();

                int width = gEnv->pRenderer->GetWidth();
                int height = gEnv->pRenderer->GetHeight();
                float projectionRatio = (float)width / (float)height;

                Matrix44A projectionMatrix, viewMatrix;

                // get the camera projection matrix
                mathMatrixPerspectiveFov(&projectionMatrix, fov, projectionRatio, nearPlane, farPlane);

                // get the camera view matrix
                mathMatrixLookAt(&viewMatrix, camera.GetPosition(), camera.GetPosition() + camera.GetViewdir(), camera.GetUp());

                // transform the world position by the view matrix
                Vec4 in, transformed, projected;
                in.x = position.GetX();
                in.y = position.GetY();
                in.z = position.GetZ();
                in.w = 1.0f;
                mathVec4Transform((f32*)& transformed, (f32*)& viewMatrix, (f32*)& in);

                // transform the world position by the projection matrix
                mathVec4Transform((f32*)& projected, (f32*)& projectionMatrix, (f32*)& transformed);

                if (projected.w < AZ_FLT_EPSILON)
                {
                    return offScreen;
                }

                projected.x /= projected.w;
                projected.y /= projected.w;

                return AZ::Vector2((1 + projected.x) * width / 2, (1 - projected.y) * height / 2);
            }

            return offScreen;
        }
    }

    AZ::Vector2 ToolKitSystemComponent::ProjectToScreen(const AZ::Vector3& position)
    {
        return Internal::ProjectVectorToScreen(position);
    }

    AZ::Vector3 ToolKitSystemComponent::ProjectEntityRadiusToScreen(const AZ::EntityId& entityId)
    {
        AZ::Vector3 offScreen(-1024.f, -1024.f, -1.f);
        if (gEnv && gEnv->pRenderer)
        {
            AZ::Aabb meshAABB;
            LmbrCentral::RenderBoundsRequestBus::EventResult(meshAABB, entityId, &LmbrCentral::RenderBoundsRequestBus::Events::GetWorldBounds);
            if (meshAABB.IsValid())
            {
                AZ::VectorFloat radius = -1.f;
                AZ::Vector3 center;
                meshAABB.GetAsSphere(center, radius);

                // get the 2D screen position of this 3D position
                AZ::Vector2 screenPosition = Internal::ProjectVectorToScreen(center);

                // get top edge of the bounding sphere relative to the camera and convert that to a 2D screen position
                const CCamera& camera = Internal::GetCamera();
                AZ::Vector2 screenPositionOffset = Internal::ProjectVectorToScreen(center + LYVec3ToAZVec3(camera.GetUp() * radius));

                // return the 2D center screenposition and the radius in screen space
                return AZ::Vector3(screenPosition.GetX(), screenPosition.GetY(), fabs(screenPositionOffset.GetY() - screenPosition.GetY()));
            }
        }
        return offScreen;

    }

    AZ::Vector4 ToolKitSystemComponent::ProjectEntityBoundsToScreen(const AZ::EntityId& entityId)
    {
        AZ::Vector4 offScreen(-1024.f, -1024.f, -1024.f, -1024.f);

        if (gEnv && gEnv->pRenderer)
        {
            AZ::Aabb meshAABB;
            LmbrCentral::RenderBoundsRequestBus::EventResult(meshAABB, entityId, &LmbrCentral::RenderBoundsRequestBus::Events::GetWorldBounds);
            if (meshAABB.IsValid())
            {
                // project each of the bound points
                AZ::Vector3 min = meshAABB.GetMin();
                AZ::Vector3 max = meshAABB.GetMax();

                AZ::Vector3 points[] = {
                    min,
                    AZ::Vector3(min.GetX(), max.GetY(), min.GetZ()),
                    AZ::Vector3(max.GetX(), max.GetY(), min.GetZ()),
                    AZ::Vector3(max.GetX(), min.GetY(), min.GetZ()),
                    AZ::Vector3(max.GetX(), min.GetY(), max.GetZ()),
                    AZ::Vector3(min.GetX(), min.GetY(), max.GetZ()),
                    AZ::Vector3(min.GetX(), max.GetY(), max.GetZ()),
                    max
                };

                AZ::Aabb aabb = AZ::Aabb::CreateNull();
                for (AZ::Vector3 point : points)
                {
                    AZ::Vector2 projectedPoint = Internal::ProjectVectorToScreen(point);
                    aabb.AddPoint(AZ::Vector3(projectedPoint.GetX(), projectedPoint.GetY(), 0.0));
                }

                return AZ::Vector4(aabb.GetMin().GetX(), aabb.GetMin().GetY(), aabb.GetMax().GetX(), aabb.GetMax().GetY());
            }
        }
        return offScreen;
    }

    AZ::Vector3 ToolKitSystemComponent::UnProjectFromScreen(const AZ::Vector2& position, float screenDepth)
    {
        if (gEnv && gEnv->pRenderer)
        {
			const CCamera& activeCamera = Internal::GetCamera();
			const float invInputY = aznumeric_cast<float>(gEnv->pRenderer->GetOverlayHeight()) - position.GetY();

			Vec3 start(0, 0, 0);
			activeCamera.Unproject(Vec3(position.GetX(), invInputY, screenDepth), start);
            return LYVec3ToAZVec3(start);
        }
        return AZ::Vector3::CreateZero();
    }

    void ToolKitSystemComponent::CloneExistingMaterial(const AZ::EntityId& entityId)
    {
        _smart_ptr<IMaterial> material;
        LmbrCentral::MaterialOwnerRequestBus::EventResult(material, entityId, &LmbrCentral::MaterialOwnerRequestBus::Events::GetMaterial);
        if (material)
        {
            _smart_ptr<IMaterial> clonedMaterial;
            clonedMaterial = gEnv->p3DEngine->GetMaterialManager()->CloneMaterial(material);
            AZ_Warning("ToolKitSystemComponent", clonedMaterial != nullptr, "Failed to clone material");
            //gEnv->p3DEngine->GetMaterialManager()->RenameMaterial(material, "cloned");
            LmbrCentral::MaterialOwnerRequestBus::Event(entityId, &LmbrCentral::MaterialOwnerRequestBus::Events::SetMaterial, clonedMaterial);
        }
    }

    void ToolKitSystemComponent::RestoreMaterial(const AZ::EntityId& entityId)
    {
        _smart_ptr<IMaterial> material;
        LmbrCentral::MaterialOwnerRequestBus::EventResult(material, entityId, &LmbrCentral::MaterialOwnerRequestBus::Events::GetMaterial);
        if (material)
        {
            // setting material to null restores the original material on the mesh
            LmbrCentral::MaterialOwnerRequestBus::Event(entityId, &LmbrCentral::MaterialOwnerRequestBus::Events::SetMaterial, nullptr);
        }
    }

    bool ToolKitSystemComponent::AABBIsVisible(const AZ::EntityId& entityId)
    {
        if (gEnv && gEnv->pSystem)
        {
            const CCamera& activeCamera = gEnv->pSystem->GetViewCamera();

            AZ::Aabb aabb = AZ::Aabb::CreateNull();
            LmbrCentral::RenderBoundsRequestBus::EventResult(aabb, entityId, &LmbrCentral::RenderBoundsRequestBus::Events::GetWorldBounds);
            if (!aabb.IsValid())
            {
                AZ::Vector3 position;
                AZ::TransformBus::EventResult(position, entityId, &AZ::TransformBus::Events::GetWorldTranslation);
                aabb = AZ::Aabb::CreateCenterRadius(position, 1.f);
            }
            return activeCamera.IsAABBVisible_F(AZAabbToLyAABB(aabb));
        }
        return false;
    }

    void ToolKitSystemComponent::SetViewShake(const AZ::Vector3& angle, const AZ::Vector3& shakeShift, float duration, float frequency, float randomness, int shakeID, bool flipVec, bool updateOnly, bool groundOnly)
    {
        if (gEnv && gEnv->pGame && gEnv->pGame->GetIGameFramework())
        {
            IGameFramework* gameFramework = gEnv->pGame->GetIGameFramework();
            IView* view = gameFramework->GetIViewSystem()->GetActiveView();
            if (view)
            {
                Ang3 convertedAngle(angle.GetX(), angle.GetY(), angle.GetZ());
                view->SetViewShake(convertedAngle, AZVec3ToLYVec3(shakeShift), duration, frequency, randomness, shakeID, flipVec, updateOnly, groundOnly);
            }
        }

    }

    // change the diffuse texture for the exiting material 
    void ToolKitSystemComponent::SetMaterialDiffuseTexture(const AZ::EntityId& entityId, const AZStd::string& texture)
    {
        _smart_ptr<IMaterial> material;
        LmbrCentral::MaterialOwnerRequestBus::EventResult(material, entityId, &LmbrCentral::MaterialOwnerRequestBus::Events::GetMaterial);
        if (material)
        {
            const SShaderItem& shaderItem = material->GetShaderItem();
            SInputShaderResources shaderResources = shaderItem.m_pShaderResources;
            if (shaderResources.GetTextureResource(EFTT_DIFFUSE))
            {
                shaderResources.m_TexturesResourcesMap[EFTT_DIFFUSE].m_Name = texture.c_str();
            }

            SShaderItem clonedShaderItem = gEnv->pRenderer->EF_LoadShaderItem(shaderItem.m_pShader->GetName(), true, 0, &shaderResources, shaderItem.m_pShader->GetGenerationMask());
            material->AssignShaderItem(clonedShaderItem);
        }
    }

    bool ToolKitSystemComponent::IsDedicatedServer()
    {
        return gEnv && gEnv->IsDedicated();
    }

    bool ToolKitSystemComponent::IsHost()
    {
        return gEnv && gEnv->bServer;
    }

    // editor
    bool ToolKitSystemComponent::IsEditor()
    {
        return gEnv && gEnv->IsEditor();
    }

    // physics
    bool ToolKitSystemComponent::IsOnGround(const AZ::EntityId& entityId)
    {
        const bool onGroundDefault = true;
        pe_status_living livingStatus;
        livingStatus.bFlying = !onGroundDefault;
        LmbrCentral::CryPhysicsComponentRequestBus::Event(entityId, &LmbrCentral::CryPhysicsComponentRequestBus::Events::GetPhysicsStatus, livingStatus);
        return !livingStatus.bFlying;
    }

    float ToolKitSystemComponent::WaterLevel(const AZ::EntityId& entityId)
    {
        if (gEnv && gEnv->p3DEngine)
        {
            // get water level at this entities location
            AZ::Transform tm = AZ::Transform::CreateZero();
            AZ::TransformBus::EventResult(tm, entityId, &AZ::TransformBus::Events::GetWorldTM);
            Vec3 pos = AZVec3ToLYVec3(tm.GetPosition());
            return gEnv->p3DEngine->GetWaterLevel(&pos);
        }

        return 0.f;
    }

    void ToolKitSystemComponent::SetVelocityAndAngularVelocity(const AZ::EntityId& entityId, const AZ::Vector3& velocity, const AZ::Vector3& angularVelocity)
    {
        IPhysicalEntity* physicalEntity = nullptr;
        LmbrCentral::CryPhysicsComponentRequestBus::EventResult(physicalEntity, entityId, &LmbrCentral::CryPhysicsComponentRequestBus::Events::GetPhysicalEntity);
        if (physicalEntity)
        {
            pe_simulation_params sim;
            sim.damping = 1.0;
            sim.dampingFreefall = 1.0;
            sim.gravity = sim.gravityFreefall = Vec3(0.f, 0.f, 0.f);
            physicalEntity->SetParams(&sim);

            pe_action_set_velocity action;
            action.v = AZVec3ToLYVec3(velocity);
            action.w = AZVec3ToLYVec3(angularVelocity);
            physicalEntity->Action(&action);
        }
    }

    void ToolKitSystemComponent::SetFreeFallDamping(const AZ::EntityId& entityId, const float damping)
    {
        IPhysicalEntity* physicalEntity = nullptr;
        LmbrCentral::CryPhysicsComponentRequestBus::EventResult(physicalEntity, entityId, &LmbrCentral::CryPhysicsComponentRequestBus::Events::GetPhysicalEntity);
        if (physicalEntity)
        {
            pe_simulation_params sim;
            sim.damping = damping;
            sim.dampingFreefall = damping;
            //sim.gravity = sim.gravityFreefall = Vec3(0.f, 0.f, 0.f);
            physicalEntity->SetParams(&sim);
        }
    }

	AZ::Vector3 ToolKitSystemComponent::GetAimDirectionForTarget(const AZ::Vector3& projectileOrigin, float projectileSpeed, const AZ::Vector3& targetPosition, const AZ::Vector3& targetVelocity)
	{
		// similar to https://www.gamedev.net/forums/topic/401165-target-prediction-system--target-leading
		const AZ::Vector3 targetDelta = targetPosition - projectileOrigin;
		float a = targetVelocity.Dot(targetVelocity) - (projectileSpeed * projectileSpeed);
		float b = targetDelta.Dot(targetVelocity) * 2.f;
		float c = targetDelta.Dot(targetDelta);

		float d = sqrt((b * b) - (4.f * a * c));

		float t0 = (-b - d) / (2.f * a);
		float t1 = (-b + d) / (2.f * a);
		if (t0 < 0.f && t1 < 0.f)
		{
			// cannot hit the target - return a zero length vector
			return AZ::Vector3(0.f, 0.f, 0.f);
		}

		float interceptTime = 0.f;
		if (t0 < 0.f)
		{
			interceptTime = t1;
		}
		else if (t1 < 0.f)
		{
			interceptTime = t0;
		}
		else
		{
			interceptTime = AZStd::min<float>(t0, t1);
		}

		const AZ::Vector3 interceptPoint = targetPosition + targetVelocity * interceptTime;
		const AZ::Vector3 aimDirection = (interceptPoint - projectileOrigin).GetNormalizedExact();
		return aimDirection;
	}

	AZStd::vector<AZ::EntityId> ToolKitSystemComponent::OverlapBox(const AZ::Transform& transform, const AZ::Vector3& dimensions, AZ::EntityId entityToIgnore)
	{
		Physics::BoxShapeConfiguration box(dimensions);
		Physics::OverlapRequest request;
		request.m_pose = transform;
		request.m_shapeConfiguration = &box;
		request.m_filterCallback = [entityToIgnore](const Physics::WorldBody* body, const Physics::Shape* shape)
		{
			return body->GetEntityId() != entityToIgnore;
		};

		AZStd::vector<Physics::OverlapHit> overlaps;
		Physics::WorldRequestBus::BroadcastResult(overlaps, &Physics::World::Overlap, request);

		AZStd::vector<AZ::EntityId> overlapIds(overlaps.size());
		AZStd::transform(overlaps.begin(), overlaps.end(), overlapIds.begin(), [](const Physics::OverlapHit& overlap)
		{
			return overlap.m_body->GetEntityId();
		});
		return overlapIds;
	}

    // console
    void ToolKitSystemComponent::ExecuteCommand(const  AZStd::string& command)
    {
        const bool silentMode = true;
        const bool waitTillNextFrame = false;

        if (gEnv && gEnv->pConsole)
        {
            gEnv->pConsole->ExecuteString(command.c_str(), silentMode, waitTillNextFrame);
        }
    }

    AZStd::string ToolKitSystemComponent::GetCVar(const  AZStd::string& cvarName)
    {
        if (gEnv && gEnv->pConsole)
        {
            ICVar *cvar = gEnv->pConsole->GetCVar(cvarName.c_str());
            if (cvar)
            {
                return AZStd::string(cvar->GetString());
            }
        }
        return AZStd::string();
    }

    // environment
    void ToolKitSystemComponent::SetOceanVisible(const bool& visible)
    {
        if (gEnv && gEnv->p3DEngine)
        {
            gEnv->p3DEngine->EnableOceanRendering(visible);
        }
    }

    void ToolKitSystemComponent::SetTerrainVisible(const bool& visible)
    {
        if (gEnv && gEnv->pConsole)
        {
            ICVar* cvar = gEnv->pConsole->GetCVar("e_Terrain");
            cvar->Set(visible);
        }
    }

    void ToolKitSystemComponent::SetTimeOfDay(const float& hour)
    {
        if (gEnv && gEnv->p3DEngine)
        {
            ITimeOfDay* timeOfDay = gEnv->p3DEngine->GetTimeOfDay();
            if (timeOfDay)
            {
                bool forceUpdate = true;
                timeOfDay->SetTime(hour, forceUpdate);
            }
        }
    }

    void ToolKitSystemComponent::LoadTimeOfDay(const AZStd::string& path)
    {
        if (gEnv && gEnv->p3DEngine)
        {
            ITimeOfDay* timeOfDay = gEnv->p3DEngine->GetTimeOfDay();
            if (timeOfDay)
            {
                string assetPath;
                assetPath.Format("@assets@/%s", path.c_str());
                XmlNodeRef root = GetISystem()->LoadXmlFromFile(assetPath);
                if (root)
                {
                    timeOfDay->Serialize(root, true);
                    timeOfDay->Update(true, true);
                }
            }
        }
    }

    void ToolKitSystemComponent::SetSkyMaterial(const AZStd::string& path)
    {
        if (gEnv && gEnv->p3DEngine)
        {
            gEnv->p3DEngine->SetSkyMaterialPath(path.c_str());
        }
    }

    void ToolKitSystemComponent::SetSunPosition(const float& longitude, const float& latitude)
    {
        if (gEnv && gEnv->p3DEngine)
        {
            ITimeOfDay* timeOfDay = gEnv->p3DEngine->GetTimeOfDay();
            if (timeOfDay)
            {
                // longitude and latidue are in degrees
                timeOfDay->SetSunPos(longitude, latitude);
                timeOfDay->Update(true, true);
            }
        }
    }
    void ToolKitSystemComponent::SetDawnDusk(const float& dawnStart, const float& dawnEnd, const float& duskStart, const float& duskEnd)
    {
        if (gEnv && gEnv->p3DEngine)
        {
            C3DEngine* engine = static_cast<C3DEngine*>(gEnv->p3DEngine);

            // these times are in minutes of the day
            engine->m_dawnStart = dawnStart;
            engine->m_dawnEnd = dawnEnd;
            engine->m_duskStart = duskStart;
            engine->m_duskEnd = duskEnd;

            ITimeOfDay* timeOfDay = gEnv->p3DEngine->GetTimeOfDay();
            if (timeOfDay)
            {
                timeOfDay->Update(true, true);
            }
        }
    }

    // UI
    AZ::Vector2 ToolKitSystemComponent::GetMousePosition(bool normalized)
    {
        AZ::Vector2 mousePosNormalized;
        AzFramework::InputSystemCursorRequestBus::BroadcastResult(mousePosNormalized, &AzFramework::InputSystemCursorRequestBus::Events::GetSystemCursorPositionNormalized);
        if (normalized)
        {
            return mousePosNormalized;
        }
        float mouseX = mousePosNormalized.GetX() * static_cast<float>(gEnv->pRenderer->GetWidth());
        float mouseY = mousePosNormalized.GetY() * static_cast<float>(gEnv->pRenderer->GetHeight());
        float invMouseY = static_cast<float>(gEnv->pRenderer->GetHeight()) - mouseY;
        return AZ::Vector2(mouseX, invMouseY);
    }

    AZ::Vector2 ToolKitSystemComponent::GetWindowResolution()
    {
        if (gEnv->pRenderer)
        {
            return AZ::Vector2(static_cast<float>(gEnv->pRenderer->GetOverlayWidth()), static_cast<float>(gEnv->pRenderer->GetOverlayHeight()));
        }

        return AZ::Vector2::CreateZero();
    }

    void ToolKitSystemComponent::OnInputChannelEvent(const AzFramework::InputChannel& inputChannel, bool& o_hasBeenConsumed)
    {
        if (inputChannel.GetInputChannelId() == AzFramework::InputDeviceTouch::Touch::Index0)
        {
            const AzFramework::InputChannel::PositionData2D* positionData2D = inputChannel.GetCustomData<AzFramework::InputChannel::PositionData2D>();
            if (positionData2D)
            {
                m_lastTouchPosition.SetX(positionData2D->m_normalizedPosition.GetX());
                m_lastTouchPosition.SetY(positionData2D->m_normalizedPosition.GetY());
                //viewportPos.SetX(positionData2D->m_normalizedPosition.GetX() * m_latestViewportSize.GetX());
                //viewportPos.SetY(positionData2D->m_normalizedPosition.GetY() * m_latestViewportSize.GetY());
            }

            //AZ::Vector2 m_lastTouchPosition = inputChannel.GetInputDevice().)
        }

    }

    int ToolKitSystemComponent::RayCastFromCursor(ray_hit &hit)
    {
        float inputX, inputY;
        const AzFramework::InputDevice* mouseDevice = AzFramework::InputDeviceRequests::FindInputDevice(AzFramework::InputDeviceMouse::Id);
        // prefer mouse
        if (mouseDevice && mouseDevice->IsConnected())
        {
            AZ::Vector2 mousePosNormalized;
            AzFramework::InputSystemCursorRequestBus::BroadcastResult(mousePosNormalized, &AzFramework::InputSystemCursorRequestBus::Events::GetSystemCursorPositionNormalized);
            inputX = mousePosNormalized.GetX() * static_cast<float>(gEnv->pRenderer->GetOverlayWidth());
            inputY = mousePosNormalized.GetY() * static_cast<float>(gEnv->pRenderer->GetOverlayHeight());
        }
        else
        {
            // use touch?
            inputX = m_lastTouchPosition.GetX() * static_cast<float>(gEnv->pRenderer->GetOverlayWidth());
            inputY = m_lastTouchPosition.GetY() * static_cast<float>(gEnv->pRenderer->GetOverlayHeight());
        }

        float invInputY = static_cast<float>(gEnv->pRenderer->GetOverlayHeight()) - inputY;

        Vec3 pos0;

        //const CCamera& activeCamera = gEnv->pRenderer->GetCamera();
        //const CCamera& activeCamera = gEnv->pSystem->GetViewCamera();
        const CCamera& activeCamera = Internal::GetCamera();

        Vec3 start(0, 0, 0);
        activeCamera.Unproject(Vec3(inputX, invInputY, 0), start);
        //gEnv->pRenderer->UnProjectFromScreen(mouseX, invMouseY, 0.01, &start.x, &start.y, &start.z);

        Vec3 end(0, 0, 0);
        activeCamera.Unproject(Vec3(inputX, invInputY, 1), end);
        //gEnv->pRenderer->UnProjectFromScreen(mouseX, invMouseY, 1, &end.x, &end.y, &end.z);

        //AZ_TracePrintf("ToolKitGem", "Start %.02f %.02f %.02f", start.x, start.y, start.z);
        Vec3 direction = (end - start).normalized() * gEnv->p3DEngine->GetMaxViewDistance();

        // if the terrain is hidden via cvar then turn off terrain collision
        // water isn't enabled as part of ent_all so we shouldn't hit that either
        int filterFlags = ent_all;
        static ICVar* terrainCVar = gEnv->pConsole->GetCVar("e_Terrain");
        //if (!gEnv->p3DEngine->GetShowTerrainSurface() || (terrainCVar && terrainCVar->GetIVal() == 0))
        if (terrainCVar && terrainCVar->GetIVal() == 0)
        {
            filterFlags &= ~ent_terrain;
        }
        const unsigned int flags = rwi_stop_at_pierceable | rwi_colltype_any | rwi_ignore_back_faces;
        int maxNumHits = 1;

        // if you are not hitting an object for some reason make sure that it has physics and a collider
        // also make sure it isn't under the terrain EVEN IF the terrain is hidden, or under water and
        // the water is hidden because even though you may not be drawing terrain/water, the physics world might
        // still see it.
        return gEnv->pPhysicalWorld->RayWorldIntersection(start, direction, filterFlags, flags, &hit, maxNumHits);
    }

    AZ::Vector3 ToolKitSystemComponent::GetPointUnderCursor()
    {
        if (gEnv->pPhysicalWorld == nullptr
            || gEnv->pRenderer == nullptr
            || gEnv->pSystem == nullptr
             )
        {
            return AZ::Vector3();
        }

        ray_hit hit;
        if (RayCastFromCursor(hit))
        {
            return LYVec3ToAZVec3(hit.pt);
        }

        return AZ::Vector3();
    }

    AZ::EntityId ToolKitSystemComponent::GetEntityUnderCursor()
    {
        if (gEnv->pPhysicalWorld == nullptr
            || gEnv->pRenderer == nullptr
            || gEnv->pSystem == nullptr
             )
        {
            return AZ::EntityId();
        }

        ray_hit hit;
        if(RayCastFromCursor(hit))
        {
            if (hit.pCollider)
            {
                if (hit.pCollider->GetiForeignData() == PHYS_FOREIGN_ID_COMPONENT_ENTITY)
                {
                    //AZ_TracePrintf("ToolKitGem", "RayCast hit an entity");
                    return static_cast<AZ::EntityId>(hit.pCollider->GetForeignData(PHYS_FOREIGN_ID_COMPONENT_ENTITY));
                }
                else
                {
                    //AZ_TracePrintf("ToolKitGem", "RayCast hit something that wasn't a component entity.");
                }
            }
        }

        //AZ_TracePrintf("ToolKitGem", "RayCast failed to hit an entity");
        return AZ::EntityId();
    }

    void ToolKitSystemComponent::DrawDebugText2D(const AZStd::string& id, const AZStd::string& text, float x, float y, float fontSize, const AZ::Vector3& color, float duration)
    {
        if (gEnv && gEnv->pGame)
        {
            auto game = gEnv->pGame->GetIGameFramework();
            if (game)
            {
                auto persistentDebug = game->GetIPersistentDebug();
                if (persistentDebug)
                {
                    const ColorF textColor(color.GetX(), color.GetY(), color.GetZ(), 1.f);
                    persistentDebug->Begin(id.c_str(), false);
                    persistentDebug->AddText(x, y, fontSize, textColor, duration, "%s", text.c_str());
                }
            }
        }
    }

    AZStd::string GetPathHelper(AZStd::string_view path)
    {
        if (!path.empty())
        {
            char fullPath[AZ_MAX_PATH_LEN] = { 0 };
            if(!AZ::IO::FileIOBase::GetInstance()->ResolvePath(path.data(), fullPath, AZ_MAX_PATH_LEN))
            {
                return path;
            }

            return AZStd::string(fullPath);
        }

        // we didn't need to resolve a path so return the executable folder
        const char* executableFolder = nullptr;
        AZ::ComponentApplicationBus::BroadcastResult(executableFolder, &AZ::ComponentApplicationBus::Events::GetExecutableFolder);
        return executableFolder ? AZStd::string(executableFolder) : AZStd::string();
    }

    AZStd::string ToolKitSystemComponent::GetPathToExe()
    {
        return GetPathHelper("");
    }

    AZStd::string ToolKitSystemComponent::GetFullPath(const AZStd::string& path)
    {
        return GetPathHelper(path);
    }

    AZStd::string ToolKitSystemComponent::GetUserPath()
    {
        return GetPathHelper("@user@");
    }
    AZStd::string ToolKitSystemComponent::GetRootPath()
    {
        return GetPathHelper("@root@");
    }
    AZStd::string ToolKitSystemComponent::GetLogPath()
    {
        return GetPathHelper("@log@");
    }
    AZStd::string ToolKitSystemComponent::GetAssetsPath()
    {
        return GetPathHelper("@assets@");
    }
    AZStd::string ToolKitSystemComponent::GetPathToAsset(const AZ::Data::AssetId& assetId)
    {
        AZStd::string path = nullptr;
        AZ::Data::AssetCatalogRequestBus::BroadcastResult(path, &AZ::Data::AssetCatalogRequests::GetAssetPathById, assetId);
        return path;
    };

    AZ::Data::AssetId ToolKitSystemComponent::GetAssetId(const AZStd::string& path)
    {
        AZ::Data::AssetId assetId;
        AZ::Data::AssetCatalogRequestBus::BroadcastResult(assetId, &AZ::Data::AssetCatalogRequests::GetAssetIdByPath, path.c_str(), AZ::Data::s_invalidAssetType, false);
        return assetId;
    };

    AZ::u64 ToolKitSystemComponent::GetModificationTime(const AZStd::string& path)
    {
        return AZ::IO::FileIOBase::GetInstance()->ModificationTime(path.c_str());
    }
}
