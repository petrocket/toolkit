
#include "StdAfx.h"
#include <platform_impl.h>

#include "ToolKitSystemComponent.h"
#include "ToolKitScriptCanvas.h"

#include <IGem.h>

namespace ToolKit
{
    class ToolKitModule
        : public CryHooksModule
    {
    public:
        AZ_RTTI(ToolKitModule, "{EFF03D6D-A58A-4F55-930A-7E0F9ECA4D2C}", CryHooksModule);

        ToolKitModule()
            : CryHooksModule()
        {
            // Push results of [MyComponent]::CreateDescriptor() into m_descriptors here.
            m_descriptors.insert(m_descriptors.end(), {
                ToolKitSystemComponent::CreateDescriptor()
            });


            // register script canvas nodes for reflection
            AZStd::vector<AZ::ComponentDescriptor*> componentDescriptors(ToolKitLibrary::GetComponentDescriptors());
            m_descriptors.insert(m_descriptors.end(), componentDescriptors.begin(), componentDescriptors.end());
        }

        /**
         * Add required SystemComponents to the SystemEntity.
         */
        AZ::ComponentTypeList GetRequiredSystemComponents() const override
        {
            return AZ::ComponentTypeList{
                azrtti_typeid<ToolKitSystemComponent>(),
            };
        }
    };
}

// DO NOT MODIFY THIS LINE UNLESS YOU RENAME THE GEM
// The first parameter should be GemName_GemIdLower
// The second should be the fully qualified name of the class above
AZ_DECLARE_MODULE_CLASS(ToolKit_2d96b1adfd044278bba7b1562bb62e1d, ToolKit::ToolKitModule)
