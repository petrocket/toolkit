
#include "StdAfx.h"


#include "ToolKitScriptCanvas.h"

#include <AzFramework/Physics/WorldBody.h>

#include <Source/ToolKitScriptCanvas.generated.cpp>

namespace ToolKit
{
	void ToolKitLibrary::Reflect(AZ::ReflectContext* reflection)
	{
		AZ::SerializeContext* serializeContext = azrtti_cast<AZ::SerializeContext*>(reflection);
		if (serializeContext)
		{
			serializeContext->Class<ToolKitLibrary, ScriptCanvas::Library::LibraryDefinition>()
				->Version(1)
				;
			AZ::EditContext* editContext = serializeContext->GetEditContext();
			if (editContext)
			{
				editContext->Class<ToolKitLibrary>("ToolKit", "")
					;
			}
		}
	}

	void ToolKitLibrary::InitNodeRegistry(ScriptCanvas::NodeRegistry& nodeRegistry)
	{
		ScriptCanvas::Library::AddNodeToRegistry<ToolKitLibrary, ToolKit::Loop>(nodeRegistry);
		ScriptCanvas::Library::AddNodeToRegistry<ToolKitLibrary, ToolKit::Loop2D>(nodeRegistry);
		//ScriptCanvas::Library::AddNodeToRegistry<ToolKitLibrary, ToolKit::All>(nodeRegistry);
		ScriptCanvas::Library::AddNodeToRegistry<ToolKitLibrary, ToolKit::GetCollisionInfo>(nodeRegistry);
	}

	AZStd::vector<AZ::ComponentDescriptor*> ToolKitLibrary::GetComponentDescriptors()
	{
		return AZStd::vector<AZ::ComponentDescriptor*>({
			ToolKit::Loop::CreateDescriptor(),
			ToolKit::Loop2D::CreateDescriptor(),
			//ToolKit::All::CreateDescriptor(),
			ToolKit::GetCollisionInfo::CreateDescriptor(),
			});
	}

	Loop::Loop()
		: Node()
	{
	}

	void  Loop::OnInputSignal(const ScriptCanvas::SlotId& slot)
	{
		if (slot != LoopProperty::GetInSlotId(this))
		{
			return;
		}

		const ScriptCanvas::SlotId outSlotId = LoopProperty::GetOutSlotId(this);
		const ScriptCanvas::Slot* valueSlot = LoopProperty::GetValueSlot(this);
		AZ_Assert(valueSlot, "Unable to retrieve value slot for Loop signal output");

		const ScriptCanvas::SlotId completeSlotId = LoopProperty::GetCompleteSlotId(this);

		const int start = LoopProperty::GetStart(this);
		const int end = LoopProperty::GetEnd(this);
		const int increment = LoopProperty::GetIncrement(this);

		ScriptCanvas::Datum valueDatum(ScriptCanvas::Data::Type::Number(), ScriptCanvas::Datum::eOriginality::Copy);
		if (increment > 0 && end > start)
		{
			for (int i = start; i <= end; i += increment)
			{
				valueDatum.Set<ScriptCanvas::Data::NumberType>(i);
				PushOutput(valueDatum, *valueSlot);
				// we must use UntilNodeIsFoundInStack or we will only get the end value and zero
				SignalOutput(outSlotId, ScriptCanvas::ExecuteMode::UntilNodeIsFoundInStack);
			}
		}
		else if (increment < 0 && end < start)
		{
			for (int i = end; i >= start; i += increment)
			{
				valueDatum.Set<ScriptCanvas::Data::NumberType>(i);
				PushOutput(valueDatum, *valueSlot);
				SignalOutput(outSlotId, ScriptCanvas::ExecuteMode::UntilNodeIsFoundInStack);
			}
		}
		SignalOutput(completeSlotId);
	}

	Loop2D::Loop2D()
		: Node()
	{
	}

	void  Loop2D::OnInputSignal(const ScriptCanvas::SlotId& slot)
	{
		if (slot != Loop2DProperty::GetInSlotId(this))
		{
			return;
		}

		int start1 = Loop2DProperty::GetStart1(this);
		int end1 = Loop2DProperty::GetEnd1(this);
		const int increment1 = Loop2DProperty::GetIncrement1(this);
		if ((increment1 > 0 && end1 < start1) || (increment1 < 0 && end1 > start1) || increment1 == 0)
		{
			// invalid start/end values
			return;
		}

		int start2 = Loop2DProperty::GetStart2(this);
		int end2 = Loop2DProperty::GetEnd2(this);
		const int increment2 = Loop2DProperty::GetIncrement2(this);
		if ((increment2 > 0 && end2 < start2) || (increment2 < 0 && end2 > start2) || increment2 == 0)
		{
			// invalid start/end values
			return;
		}

		const ScriptCanvas::SlotId outSlotId = Loop2DProperty::GetOutSlotId(this);
		const ScriptCanvas::Slot* value1Slot = Loop2DProperty::GetValue1Slot(this);
		const ScriptCanvas::Slot* value2Slot = Loop2DProperty::GetValue2Slot(this);
		const ScriptCanvas::SlotId completeSlotId = Loop2DProperty::GetCompleteSlotId(this);

		AZ_Assert(value1Slot, "Unable to retrieve value 1 slot for Loop signal output");
		AZ_Assert(value2Slot, "Unable to retrieve value 2 slot for Loop signal output");

		ScriptCanvas::Datum value1Datum(ScriptCanvas::Data::Type::Number(), ScriptCanvas::Datum::eOriginality::Copy);
		ScriptCanvas::Datum value2Datum(ScriptCanvas::Data::Type::Number(), ScriptCanvas::Datum::eOriginality::Copy);
		auto iterate = [&](int i, int j) {
			value1Datum.Set<ScriptCanvas::Data::NumberType>(i);
			PushOutput(value1Datum, *value1Slot);

			value2Datum.Set<ScriptCanvas::Data::NumberType>(j);
			PushOutput(value2Datum, *value2Slot);

			// we must use UntilNodeIsFoundInStack or we will only get the end value and zero
			SignalOutput(outSlotId, ScriptCanvas::ExecuteMode::UntilNodeIsFoundInStack);
		};

		if (increment1 > 0 && end1 > start1)
		{
			for (int i = start1; i <= end1; i += increment1)
			{
				if (increment2 > 0 && end2 > start2)
				{
					for (int j = start2; j <= end2; j += increment2)
					{
						iterate(i, j);
					}
				}
				else if (increment2 < 0 && end2 < start2)
				{
					for (int j = start2; j >= end2; j += increment2)
					{
						iterate(i, j);
					}
				}
			}
		}
		else if (increment1 < 0 && end1 < start1)
		{
			for (int i = end1; i >= start1; i += increment1)
			{
				if (increment2 > 0 && end2 > start2)
				{
					for (int j = start2; j <= end2; j += increment2)
					{
						iterate(i, j);
					}
				}
				else if (increment2 < 0 && end2 < start2)
				{
					for (int j = start2; j >= end2; j += increment2)
					{
						iterate(i, j);
					}
				}
			}
		}
		SignalOutput(completeSlotId);
	}

	/*
	All::All()
		: Node()
        , m_triggeredOutput(false)
		, m_resetSlotId(ScriptCanvas::SlotId())
	{
	}

	void All::OnActivate()
	{
		m_triggeredOutput = false;
		m_triggeredInputs.clear();
	}

	void All::OnInit()
	{
		m_resetSlotId = AllProperty::GetResetSlotId(this);
	}

	void All::OnConfigured()
	{
		AZStd::vector< const ScriptCanvas::Slot* > slots = GetAllSlotsByDescriptor(ScriptCanvas::SlotDescriptors::ExecutionIn());
		if (slots.size() <= 1)
		{
			AddInputSlot();
		}
	}

	bool All::CanDeleteSlot(const ScriptCanvas::SlotId& slotId) const
	{
		if (slotId == m_resetSlotId)
		{
			return false;
		}

		ScriptCanvas::Slot* slot = GetSlot(slotId);
		if (slot && slot->IsExecution() && slot->IsInput())
		{
			auto slots = GetAllSlotsByDescriptor(ScriptCanvas::SlotDescriptors::ExecutionIn());
			return slots.size() > 2;
		}
		else
		{
			return false;
		}
	}

	void All::OnSlotRemoved(const ScriptCanvas::SlotId& slotId)
	{
		FixupStateNames();
	}

	int All::GetNumberOfExtensions() const
	{
		return 1;
	}

	ScriptCanvas::SlotId All::HandleExtension(AZ::Crc32 extensionId)
	{
		if (extensionId == GetInputExtensionId())
		{
			return AddInputSlot();
		}

		return ScriptCanvas::SlotId();
	}

	ScriptCanvas::ExtendableSlotConfiguration All::GetExtensionConfiguration(int extensionCount) const
	{
		ScriptCanvas::ExtendableSlotConfiguration extendableConfiguration;

		extendableConfiguration.m_name = "Add Input";
		extendableConfiguration.m_tooltip = "Adds a new input to the Any Node";

		// DisplayGroup Taken from GraphCanvas
		extendableConfiguration.m_displayGroup = "SlotGroup_Execution";

		extendableConfiguration.m_connectionType = ScriptCanvas::ConnectionType::Input;
		extendableConfiguration.m_identifier = GetInputExtensionId();

		return extendableConfiguration;

	}

	void All::OnInputSignal(const ScriptCanvas::SlotId& slotId)
	{
		if (slotId == m_resetSlotId)
		{
			m_triggeredOutput = false;
			m_triggeredInputs.clear();
			return;
		}
		else if (m_triggeredOutput)
		{
			// ignore subsequent inputs if we already sent an output signal
			return;
		}

		auto inputSlots = GetAllSlotsByDescriptor(ScriptCanvas::SlotDescriptors::ExecutionIn());
		size_t inputCount = inputSlots.size();

		AZ_Assert(inputCount > 0, "All node input count should never be zero");

		// subtract one for the reset input slot
		inputCount--;
		m_triggeredInputs.insert(slotId);
		if (m_triggeredInputs.size() == inputCount)
		{
			// ignore subsequent input signals
			m_triggeredOutput = true;

			const ScriptCanvas::SlotId outSlotId = AllProperty::GetOutSlotId(this);
			SignalOutput(outSlotId);
		}
	}

	AZStd::string All::GenerateInputName(int counter)
	{
		return AZStd::string::format("Input %i", counter);
	}

	ScriptCanvas::SlotId All::AddInputSlot()
	{
		auto inputSlots = GetAllSlotsByDescriptor(ScriptCanvas::SlotDescriptors::ExecutionIn());
		int inputCount = static_cast<int>(inputSlots.size());

		ScriptCanvas::ExecutionSlotConfiguration  slotConfiguration(GenerateInputName(inputCount), ScriptCanvas::ConnectionType::Input);
		return AddSlot(slotConfiguration);
	}

	void All::FixupStateNames()
	{
		auto inputSlots = GetAllSlotsByDescriptor(ScriptCanvas::SlotDescriptors::ExecutionIn());
		// start at input 1 because the first input is for the reset
		for (int i = 1; i < inputSlots.size(); ++i)
		{
			ScriptCanvas::Slot* slot = GetSlot(inputSlots[i]->GetId());

			if (slot)
			{
				slot->Rename(GenerateInputName(i));
			}
		}
	}
	*/

	void GetCollisionInfo::OnInputSignal(const ScriptCanvas::SlotId& slotId)
	{
		if (slotId == GetCollisionInfoProperty::GetInSlotId(this))
		{
			ScriptCanvas::Datum entityIdData(ScriptCanvas::Data::Type::EntityID(), ScriptCanvas::Datum::eOriginality::Copy);

			const ScriptCanvas::Slot * entityIdSlot = GetCollisionInfoProperty::GetEntityIdSlot(this);
			const AzFrameworkCollision collision = GetCollisionInfoProperty::GetCollision(this);

			if (collision.m_otherBody)
			{
				entityIdData.Set<ScriptCanvas::Data::EntityIDType>(collision.m_otherBody->GetEntityId());
			}
			else
			{
				entityIdData.Set<ScriptCanvas::Data::EntityIDType>(AZ::EntityId());
			}
			PushOutput(entityIdData, *entityIdSlot);

			const ScriptCanvas::SlotId outSlotId = GetCollisionInfoProperty::GetOutSlotId(this);
			SignalOutput(outSlotId);
		}
	}
}
