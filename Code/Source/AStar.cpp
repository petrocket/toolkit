#include "StdAfx.h"
#include <ToolKit/AStar.h>


#include <AzCore/Serialization/SerializeContext.h>
#include <AzCore/Serialization/EditContext.h>
#include <AzCore/RTTI/BehaviorContext.h>
#include <AzCore/Casting/numeric_cast.h>

#pragma optimize("", off)

namespace ToolKit
{
	AStarSolver::AStarSolver(AZ::u16 gridWidth, AZ::u16 gridHeight)
	{
		const size_t width = aznumeric_cast<size_t>(gridWidth);
		const size_t height  = aznumeric_cast<size_t>(gridHeight);
		constexpr size_t maxGridSize = std::numeric_limits<AStarNodeId>::max();
		AZ_Assert((width * height) < maxGridSize, "Grid size is too large for AStarSolver");

		m_grid.m_width = gridWidth;
		m_grid.m_height = gridHeight;
		m_grid.m_nodes.resize_no_construct(width * height);
		memset(m_grid.m_nodes.data(), 0, m_grid.m_nodes.size() * sizeof(AStarNode));
	}

	void AStarSolver::Reflect(AZ::ReflectContext* context)
	{

	}

	void AStarSolver::SetObstacle(AStarNodeId nodeId, bool isObstacle)
	{
		AZ_Assert(nodeId < m_grid.m_nodes.size(), "Invalid nodeId");
		m_grid.m_nodes[nodeId].m_isObstacle = isObstacle;
	}

	void AStarSolver::ClearObstacles()
	{
		memset(m_grid.m_nodes.data(), 0, m_grid.m_nodes.size() * sizeof(AStarNode));
	}

	AZ::u16 AStarSolver::GetCostBasedOnDistance(AStarNodeId startNodeId, AStarNodeId endNodeId)
	{
		const AZ::VectorFloat distanceMultiplier(64.f);

		// @TODO prebuild a table of costs, for now calculate on the fly

		const AStarNodePoint start = GetNodePosition(startNodeId);
		const AStarNodePoint end = GetNodePosition(endNodeId);

		const AZ::VectorFloat startX = AZStd::get<0>(start);
		const AZ::VectorFloat startY = AZStd::get<1>(start);

		const AZ::VectorFloat endX = AZStd::get<0>(end);
		const AZ::VectorFloat endY = AZStd::get<1>(end);

		const AZ::VectorFloat dx = startX > endX ? startX - endX : endX - startX;
		const AZ::VectorFloat dy = startY > endY ? startY - endY : endY - startY;
		AZ::VectorFloat delta = dx * dx + dy * dy;

		return aznumeric_cast<AZ::u16>(static_cast<float>(delta.GetSqrtApprox() * distanceMultiplier)) ;
	}

	bool AStarSolver::NeighborIsOffGrid(AStarNodeId nodeId, int direction)
	{
		const AZ::u16 x = nodeId % m_grid.m_width;
		const AZ::u16 y = (nodeId - x) / m_grid.m_width;

		// direction is integer value that starts at noon on a clock and increments clockwise at 45 degree angles
		// 7 0 1
		// 6   2
		// 5 4 3
		//
		return
			((direction == 0 || direction == 1 || direction == 7) && y == 0) ||                    // top
			((direction == 5 || direction == 6 || direction == 7) && x == 0) ||                    // left
			((direction == 1 || direction == 2 || direction == 3) && x == (m_grid.m_width - 1)) || // right
			((direction == 3 || direction == 4 || direction == 5) && y == (m_grid.m_height - 1));  // bottom
	}

	AStarNodeId AStarSolver::GetNodeId(AStarNode* node)
	{
		AZ_Assert(node && ((node - m_grid.m_nodes.data()) < m_grid.m_nodes.size()), "Invalid node in GetNodeId");
		AStarNode* firstNode = m_grid.m_nodes.data();
		return aznumeric_cast<AStarNodeId>(node - firstNode);
	}

	AZStd::vector<AStarNodeId> AStarSolver::FindPath(AStarNodeId startNodeId, AStarNodeId endNodeId)
	{
		constexpr AStarNodeId INVALID_NODE = std::numeric_limits<AStarNodeId>::max();

		if (startNodeId >= m_grid.m_nodes.size() || endNodeId >= m_grid.m_nodes.size())
		{
			AZ_Printf("AStarSolver", "Invalid start or end node given to FindPath");
			return AZStd::vector<AStarNodeId>();
		}

		// reset all visited (@TODO move visited to bit vector so can memset?)
		for (auto& node : m_grid.m_nodes)
		{
			node.m_visited = false;
		}

		// init the original grid node
		AStarNode* currentNode = &(m_grid.m_nodes[startNodeId]);
		currentNode->m_inOpenList = true;
		currentNode->m_visited = true;
		currentNode->m_nextOpenListNode = INVALID_NODE;
		currentNode->m_prevOpenListNode = INVALID_NODE;
		currentNode->m_prevPathNode = INVALID_NODE;

		// cost
		currentNode->m_costFromStart = 0;
		currentNode->m_totalEstimatedCost = GetCostBasedOnDistance(startNodeId, endNodeId);

		AStarNode* firstOpenNode = currentNode;
		AStarNode* lastOpenNode = currentNode;
		AStarNode* endNode = &(m_grid.m_nodes[endNodeId]);

		// direction is integer value that starts at noon on a clock and increments clockwise at 45 degree angles
		// 7 0 1
		// 6   2
		// 5 4 3
		//
		const int directionOffsets[8] =
		{
			-m_grid.m_width,        // up
			-m_grid.m_width + 1,  // up right
			1,                    // right
			m_grid.m_width + 1,   // bottom right
			m_grid.m_width,         // bottom
			m_grid.m_width - 1,   // bottom left
			-1,                   // left
			-m_grid.m_width - 1   // top left
		};

		// loop through the open (frontier) list
		while (firstOpenNode)
		{
			// find the lowest cost node in the open (frontier) list
			AStarNode* lowestCostNode = firstOpenNode;
			for (AStarNodeId nodeId = lowestCostNode->m_nextOpenListNode; nodeId != INVALID_NODE; nodeId = m_grid.m_nodes[nodeId].m_nextOpenListNode)
			{
				AStarNode* node = &(m_grid.m_nodes[nodeId]);
				if (node->m_totalEstimatedCost < lowestCostNode->m_totalEstimatedCost)
				{
					lowestCostNode = node;
				}
			}
			currentNode = lowestCostNode;

			// if we arrived at the goal build the path and return it 
			if (currentNode == endNode)
			{
				// @TODO determine the size and allocate an array instead of inserting
				AZStd::vector<AStarNodeId> path;
				AStarNodeId currentNodeId = endNodeId;
				while (currentNodeId != INVALID_NODE)
				{
					path.emplace(path.begin(), currentNodeId);
					currentNodeId = m_grid.m_nodes[currentNodeId].m_prevPathNode;
				}
				return path;
			}

			// remove this node from the open list (frontier) and fix up next/prev
			{
				currentNode->m_inOpenList = false;
				if (currentNode->m_prevOpenListNode != INVALID_NODE)
				{
					m_grid.m_nodes[currentNode->m_prevOpenListNode].m_nextOpenListNode = currentNode->m_nextOpenListNode;
				}
				else if (currentNode->m_nextOpenListNode != INVALID_NODE)
				{
					firstOpenNode = &(m_grid.m_nodes[currentNode->m_nextOpenListNode]);
				}
				else
				{
					firstOpenNode = nullptr;
				}

				if (currentNode->m_nextOpenListNode != INVALID_NODE)
				{
					m_grid.m_nodes[currentNode->m_nextOpenListNode].m_prevOpenListNode = currentNode->m_prevOpenListNode;
				}
				else if (currentNode->m_prevOpenListNode != INVALID_NODE)
				{
					lastOpenNode = &(m_grid.m_nodes[currentNode->m_prevOpenListNode]);
				}
				else
				{
					lastOpenNode = nullptr;
				}
			}

			const AStarNodeId currentNodeId = GetNodeId(currentNode);

			// add all valid neighbors to the open list and update their costs
			for (int direction = 0; direction < 8; direction++)
			{
				// skip if the neighbor in this direction is off the grid 
				if (NeighborIsOffGrid(currentNodeId, direction))
				{
					continue;
				}

				const AStarNodeId neighborId = currentNodeId + directionOffsets[direction];
				AStarNode* neighbor = &(m_grid.m_nodes[neighborId]);

				// skip if the neighbor is an obstacle
				if (neighbor->m_isObstacle)
				{
					continue;
				}

				// the cost to move to a diagonal node is more
				constexpr AZ::u16 regularMoveCost = 64;
				constexpr AZ::u16 diagonalMoveCost = aznumeric_cast<AZ::u16>(64.0 * 1.4142);

				// calculate the cost to move to this neighbor
				const AZ::u16 moveCost = direction % 2 == 0 ? regularMoveCost : diagonalMoveCost;
				const AZ::u16 newCostFromStart = currentNode->m_costFromStart + moveCost;

				const bool visited = neighbor->m_visited;

				// skip if we have visited this neighbor and cost was less
				if (visited && neighbor->m_costFromStart <= newCostFromStart)
				{
					continue;
				}
				
				// note that we visited this neighbor
				neighbor->m_visited = true;
				neighbor->m_prevPathNode = currentNodeId;
				neighbor->m_costFromStart = newCostFromStart;
				neighbor->m_totalEstimatedCost = GetCostBasedOnDistance(neighborId, endNodeId);

				if (!visited || !neighbor->m_inOpenList)
				{
					// add to the open list
					neighbor->m_inOpenList = true;
					neighbor->m_prevOpenListNode = lastOpenNode ? GetNodeId(lastOpenNode) : INVALID_NODE;
					neighbor->m_nextOpenListNode = INVALID_NODE;
					if (neighbor->m_prevOpenListNode != INVALID_NODE)
					{
						m_grid.m_nodes[neighbor->m_prevOpenListNode].m_nextOpenListNode = neighborId;
					}
					lastOpenNode = neighbor;
					if (!firstOpenNode)
					{
						firstOpenNode = neighbor;
					}
				}
			}
		}

		// return an empty vector
		return AZStd::vector<AStarNodeId>();
	}
}